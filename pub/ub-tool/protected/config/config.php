<?php
return array(
    'components'=>array(
        //Database of Magento1
        'mage1' => array(
            'connectionString' => 'mysql:host=localhost;port=3306;dbname=safechecklive',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '@123My0Sql@Em!zen#@',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'class' => 'CDbConnection'
        ),
        //Database of Magento2 beta
        'mage2' => array(
            'connectionString' => 'mysql:host=localhost;port=3306;dbname=safe2',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '@123My0Sql@Em!zen#@',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'class' => 'CDbConnection'
        )
    ),
    //'import'=>array(
        //This can change for your magento1 version if needed
    //    'application.models.mage19x.*'
    //)
);
