<?php $this->pageTitle = $step->title . ' - ' . Yii::app()->name; ?>

<h1 class="page-header"> Step <?php echo $step->sorder?>: <?php echo $step->title?> </h1>

<?php

$migrated_object_ids = isset(Yii::app()->session['migrated_other_object_ids']) ? Yii::app()->session['migrated_other_object_ids'] : array();

$migrated_website_ids = isset(Yii::app()->session['migrated_website_ids']) ? Yii::app()->session['migrated_website_ids'] : array();
$str_website_ids = implode(',', $migrated_website_ids);

$migrated_customer_group_ids = isset(Yii::app()->session['migrated_customer_group_ids']) ? Yii::app()->session['migrated_customer_group_ids'] : array();
$str_customer_group_ids = implode(',', $migrated_customer_group_ids);

$migrated_product_ids = isset(Yii::app()->session['migrated_product_ids']) ? Yii::app()->session['migrated_product_ids'] : array();
$str_product_ids = implode(',', $migrated_product_ids);

?>

<form role="form" method="post" action="<?php echo Yii::app()->createUrl("migrate/step{$step->sorder}"); ?>">
    <div id="step-content">
        <blockquote> <p class="tip"> <?php echo Yii::t('frontend', $step->descriptions); ?> </p> </blockquote>

        <?php $this->renderPartial('_frmButtons', array('step' => $step)); ?>

        <ul class="list-group">
            <li class="list-group-item">
                <h3 class="list-group-item-heading">
                    <input type="checkbox" id="select-all" name="select_all_object" title="<?php echo Yii::t('frontend', 'Click here to select all.')?>" />
                    <?php echo Yii::t('frontend', 'Other Data'); ?>
                </h3>
                <?php if (isset($objects) && $objects): ?>
                    <ul class="list-group">
                        <?php foreach ($objects as $id => $label): ?>
                            <li class="list-group-item">
                                <h4 class="list-group-item-heading">
                                    <?php if ($checked = in_array($id, $migrated_object_ids)): ?>
                                        <span class="glyphicon glyphicon-ok-sign text-success"></span>
                                    <?php endif; ?>
                                    <input type="checkbox" <?php echo ($checked) ? "checked" : ''; ?> id="object_<?php echo $id; ?>" name="selected_objects[]" value="<?php echo $id; ?>" />
                                    <?php
                                    $total = null;
                                    if ($id == 'tax') {
                                        $total = Mage1TaxCalculationRule::model()->count().", " . Mage1TaxCalculationRate::model()->count();
                                    } else if ($id == 'catalog_rule') {
                                        $total = Mage1Catalogrule::model()->count();
                                    } else if ($id == 'group_price') {
                                        if ($str_customer_group_ids) {
                                            $condition = "entity_id IN ({$str_product_ids}) AND customer_group_id IN ({$str_customer_group_ids}) AND website_id IN ($str_website_ids)";
                                            $total = Mage1CatalogProductEntityGroupPrice::model()->count($condition);
                                        } else {
                                            $total = 0;
                                        }
                                        if ($total) {
                                            $total .= ', All Product group prices will be convert to Product tier prices';
                                        }
                                    } else if ($id == 'tier_price') {
                                        if ($str_customer_group_ids) {
                                            $condition = "entity_id IN ({$str_product_ids}) AND customer_group_id IN ({$str_customer_group_ids}) AND website_id IN ($str_website_ids)";
                                            $total = Mage1CatalogProductEntityTierPrice::model()->count($condition);
                                        } else {
                                            $total = 0;
                                        }
                                    }
                                    $enfix = (!is_null($total)) ?  " (". $total .")" : '';
                                    ?>
                                    <span> <?php echo $label . $enfix; ?> </span>
                                </h4>
                                <?php if ($id == 'email_template_newsletter'):?>
                                    <ul class="list-group">
                                        <li class="list-group-item"><span>- <?php echo Yii::t('frontend', 'Email Templates') .' ('.Mage1EmailTemplate::model()->count().')' ?></span></li>
                                        <li class="list-group-item"><span>- <?php echo Yii::t('frontend', 'Newsletter Templates') .' ('.Mage1NewsletterTemplate::model()->count().')' ?></span></li>
                                        <li class="list-group-item"><span>- <?php echo Yii::t('frontend', 'Newsletter Queue') .' ('.Mage1NewsletterQueue::model()->count().')' ?></span></li>
                                        <li class="list-group-item"><span>- <?php echo Yii::t('frontend', 'Newsletter Subscribers') .' ('.Mage1NewsletterSubscriber::model()->count().')' ?></span></li>
                                        <li class="list-group-item"><span>- <?php echo Yii::t('frontend', 'Newsletter Problem Reports') .' ('.Mage1NewsletterProblem::model()->count().')' ?></span></li>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        </ul>
    </div>

    <?php $this->renderPartial('_frmButtons', array('step' => $step)); ?>
</form>