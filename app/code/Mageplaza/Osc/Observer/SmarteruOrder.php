<?php

namespace  Mageplaza\Osc\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SmarteruOrder implements ObserverInterface {
	protected $_curlFactory;
	protected $_curl;
	public $_storeManager;
	protected $_quote;
	protected $_country;
	protected $_logger;

	public function __construct(
      	\Magento\Store\Model\StoreManagerInterface $storeManager,
      	\Magento\Framework\HTTP\Client\Curl $curl,
      	\Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
      	\Magento\Quote\Model\Quote $quote,
      	\Magento\Directory\Model\Country $country,
      	\Psr\Log\LoggerInterface $logger
    ) {
        $this->_storeManager = $storeManager;
     	$this->_curl = $curl;
     	$this->_curlFactory = $curlFactory;
     	$this->_quote = $quote;
     	$this->_country = $country;
     	$this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

    	try{
        	$invoice = $observer->getEvent()->getInvoice();
        	$order = $invoice->getOrder();

        	if($invoice->getState() != \Magento\Sales\Model\Order\Invoice::STATE_PAID) {
        		$this->_logger->addInfo("Invoice not paid:" . $invoice->getId());
        		return;
        	}
        	// $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	        // $order_detail = $objectManager->get('Magento\Sales\Model\Order');
	        // $order_information = $order_detail->loadByIncrementId($order->getIncrementId());

			// Line Items
			$su_lineitems = array();
		 	foreach ($order->getAllItems() as $item) {
	            $produst_type = $item->getProductType();
	            if($produst_type != 'virtual'){
	            	continue;
	            }
	            $su_lineitem = array();
				$su_lineitem['SKU'] = $item->getSku();
				$su_lineitem['description'] = $item->getName();
				$su_lineitem['quantity'] = $item->getQtyOrdered();
				$su_lineitem['unitPrice'] = $item->getPriceInclTax();
				$su_lineitem['subtotalPrice'] = $item->getPriceInclTax();
				$su_lineitem['totalTax'] = $item->getPriceInclTax();
				$su_lineitem['totalPrice'] = $item->getPriceInclTax();

				array_push($su_lineitems, $su_lineitem);
	        }
	        
	        if (!count($su_lineitems)) {
	        	$this->_logger->addInfo("no item virutal in order id: " . $order->getId());
				return;
			}

			$su_order['lineItems'] = $su_lineitems;
				
			$su_order['orderID'] = (int)$order->getId();
			$su_order['orderDate'] = $order->getCreatedAt();
			$su_order['orderNumber'] = (int)$order->getId();
			$su_order['subtotalPrice'] = $order->getSubtotal();
			$su_order['totalPrice'] = $order->getGrandTotal();
			$su_order['totalTax'] = $order->getTaxAmount();

		 	// $objectManager_new = \Magento\Framework\App\ObjectManager::getInstance();
            $quote = $this->_quote->load($order->getQuoteId());
            $studentAddress = $quote->getStudentAddress();

            $this->_logger->addInfo("student have mail :".$studentAddress->getEmail());

            if (!$studentAddress && $studentAddress->getEmail() == '') {
            	$this->_logger->addError("student have not any mail due to same as billing:".$student_address->getId());
            	$studentAddress = $order->getBillingAddress();
            }

            if ($studentAddress->getId() != '') {
				$student = array();
				$student['address1'] = $studentAddress->getStreetFull();
				$student['city'] = $studentAddress->getCity();
				$student['company'] = $studentAddress->getCompany();
				$student['country'] = $this->_country->loadByCode($studentAddress->getCountryId())->getName();
				$student['firstName'] = strrev($studentAddress->getFirstname());
				$student['lastName'] = strrev($studentAddress->getLastname());
				$student['phone'] = $studentAddress->getTelephone();
				$student['province'] = $studentAddress->getRegion();
				$student['postcode'] = $studentAddress->getPostcode();

				$su_order['billingAddress'] = $student;

				$customer = array();
				// $customer['email'] = $studentAddress->getEmail();
				// $customer['firstName'] = $studentAddress->getFirstname();
				// $customer['lastName'] = $studentAddress->getLastname();

				$customer['email'] = $studentAddress->getEmail();
				$customer['firstName'] = $studentAddress->getFirstname();
				$customer['lastName'] = $studentAddress->getLastname();
				$customer['address1'] = $studentAddress->getStreetFull();
				$customer['city'] = $studentAddress->getCity();
				$customer['province'] = $studentAddress->getRegion();
				$customer['company'] = $studentAddress->getCompany();
				$customer['phone'] = $studentAddress->getTelephone();
				$customer['postcode'] = $studentAddress->getPostcode();
			
				$su_order['customer'] = $customer;

				// POST to SmarterU
				$post_data = json_encode($su_order, JSON_PRETTY_PRINT);
				$baseUrlDomain =  $this->_storeManager->getStore()->getBaseUrl();
				$baseUrlDomain = str_replace('https://', 'http://', $baseUrlDomain);

				$this->_logger->addInfo("Post data smarteru: ". $post_data . " -- domain: ". $baseUrlDomain);

				$headers = array(
					'Content-Type: application/json',
					'X-Magento-Domain: '.$baseUrlDomain,
					'Content-Length: '.strlen($post_data)
				);

				$ch = curl_init('https://app.smarteru.com/eCommerce/Magento/');                                                                      
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch,CURLOPT_TIMEOUT,10);
				$result = curl_exec($ch);
				curl_close($ch);

				$this->_logger->addInfo("Result smarteru: ". $result);
				
				// $httpAdapter = $this->_curlFactory->create();

		  //       $postbackUrl = "https://app.smarteru.com/eCommerce/Magento/";


		  //       $httpAdapter->setConfig(['timeout'   => 10]);
		  //       $httpAdapter->write(\Zend_Http_Client::POST, $postbackUrl, '1.1', $headers, $post_data);
		        
		  //       $postbackResult = $httpAdapter->read();
		  //       $httpAdapter->close();

			}
		}catch(\Exception $e) {
			$this->_logger->critical($e);
		}	
    }
}