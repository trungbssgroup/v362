<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\Osc\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Framework\Model\AbstractExtensibleModel;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Model\ResourceModel;
use Magento\Sales\Model\Status;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;

class Quote extends \Magento\Quote\Model\Quote
{
	/**
     * @param \Magento\Quote\Api\Data\AddressInterface $address
     * @return $this
     */
    public function setStudentAddress(\Magento\Quote\Api\Data\AddressInterface $address = null)
    {
        $old = $this->getStudentAddress();

        if (!empty($old)) {
            $old->addData($address->getData());
        } else {
            $this->addAddress($address->setAddressType('student'));
        }
        return $this;
    }

    /**
     * Retrieve quote billing address
     *
     * @return Address
     */
    public function getStudentAddress()
    {
        return $this->_getAddressByType('student');
    }
}
