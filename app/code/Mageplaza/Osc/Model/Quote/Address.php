<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\Osc\Model\Quote;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Quote\Api\Data\AddressInterface;

class Address extends \Magento\Quote\Model\Quote\Address
{
	 /**
     * {@inheritdoc}
     */
    public function getSuite()
    {
        return $this->getData(self::KEY_SUITE);
    }

     /**
     * {@inheritdoc}
     */
    public function setSuite($suite)
    {
        return $this->setData(self::KEY_SUITE, $suite);
    }
}