/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

var config = {};
if (window.location.href.indexOf('onestepcheckout') !== -1) {
    config = {
        map: {
            '*': {
                'Magento_Checkout/js/model/shipping-rate-service': 'Mageplaza_Osc/js/model/shipping-rate-service',
                'Magento_Checkout/js/model/shipping-rates-validator': 'Mageplaza_Osc/js/model/shipping-rates-validator',
                'Magento_SalesRule/template/payment/discount': 'Mageplaza_Osc/template/container/review/discount',
                'Magento_Tax/template/checkout/summary/grand-total': 'Mageplaza_Osc/template/container/summary/grand-total',
                checkoutLoader: 'Mageplaza_Osc/js/checkout-loader',
                'Magento_Checkout/js/checkout-data': 'Mageplaza_Osc/js/checkout-data',
                'Magento_Checkout/js/model/new-customer-address': 'Mageplaza_Osc/js/model/new-customer-address',
                'Magento_Paypal/js/view/payment/method-renderer/payflowpro-method' : 'Mageplaza_Osc/js/view/payment/method-renderer/payflowpro-method',
                'Magento_Paypal/js/view/payment/method-renderer/in-context/checkout-express' : 'Mageplaza_Osc/js/view/payment/method-renderer/checkout-express'

            },
            'Mageplaza_Osc/js/model/shipping-rates-validator': {
                'Magento_Checkout/js/model/shipping-rates-validator': 'Magento_Checkout/js/model/shipping-rates-validator'
            },
            'Magento_Checkout/js/model/shipping-save-processor/default': {
                'Magento_Checkout/js/model/full-screen-loader': 'Mageplaza_Osc/js/model/osc-loader'
            },
            'Magento_Checkout/js/action/set-billing-address': {
                'Magento_Checkout/js/model/full-screen-loader': 'Mageplaza_Osc/js/model/osc-loader'
            },
            'Mageplaza_Osc/js/model/osc-loader': {
                'Magento_Checkout/js/model/full-screen-loader': 'Magento_Checkout/js/model/full-screen-loader'
            }
        }
    };
}