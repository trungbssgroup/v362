/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/view/billing-address',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Mageplaza_Osc/js/model/osc-data',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/set-billing-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Ui/js/model/messageList',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/address/auto-complete',
        'uiRegistry',
        'mage/translate',
        'rjsResolver',
        'Magento_Customer/js/model/address-list'
    ],
    function ($,
              ko,
              Component,
              quote,
              checkoutData,
              oscData,
              createBillingAddress,
              selectBillingAddress,
              customer,
              setBillingAddressAction,
              addressConverter,
              additionalValidators,
              globalMessageList,
              checkoutDataResolver,
              addressAutoComplete,
              registry,
              $t,
              resolver,
              addressList) {
        'use strict';

        var observedElements = [],
            canShowBillingAddress = window.checkoutConfig.oscConfig.showBillingAddress;
        var newAddressOption = {
                getAddressInline: function () {
                return $t('New Address');
            },
                customerAddressId: null
            },
            addressOptions = addressList().filter(function (address) {
                return address.getType() == 'customer-address';
            });

        addressOptions.push(newAddressOption);

        return Component.extend({
            defaults: {
                template: ''
            },
            quoteIsVirtual: quote.isVirtual(),

            canUseShippingAddress: ko.computed(function () {
                return !quote.isVirtual() && quote.shippingAddress() &&
                    quote.shippingAddress().canUseForBilling() && canShowBillingAddress;
            }),

            /**
             * @return {exports}
             */
            initialize: function () {
                var self = this;

                this._super();

                this.initFields();

                this.initSameStudent();

                additionalValidators.registerValidator(this);

                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var billingAddressData = checkoutData.getBillingAddressFromData();

                    if (billingAddressData) {
                        checkoutProvider.set(
                            'billingAddress',
                            $.extend({}, checkoutProvider.get('billingAddress'), billingAddressData)
                        );
                    }
                    checkoutProvider.on('billingAddress', function (billingAddressData) {
                        checkoutData.setBillingAddressFromData(billingAddressData);
                    });
                });

                quote.shippingAddress.subscribe(function (newAddress) {
                    if (self.isAddressSameAsShipping()) {
                        selectBillingAddress(newAddress);
                    }
                });

                resolver(this.afterResolveDocument.bind(this));

                return this;
            },

            /**
             * @return {exports.initObservable}
             */
            initObservable: function () {
                this._super()
                    .observe({
                        selectedAddress: null,
                        isAddressDetailsVisible: quote.billingAddress() != null,
                        isAddressFormVisible: !customer.isLoggedIn() || addressOptions.length == 1,
                        isAddressSameAsShipping: false,
                        isAddressSameAsStudent : false,
                        saveInAddressBook: 1
                    });

                quote.billingAddress.subscribe(function (newAddress) {
                    if (quote.isVirtual()) {
                        this.isAddressSameAsShipping(false);
                    } else {
                        this.isAddressSameAsShipping(
                            newAddress != null &&
                            newAddress.getCacheKey() == quote.shippingAddress().getCacheKey()
                        );
                    }

                    if (newAddress != null && newAddress.saveInAddressBook !== undefined) {
                        this.saveInAddressBook(newAddress.saveInAddressBook);
                    } else {
                        this.saveInAddressBook(1);
                    }
                    this.isAddressDetailsVisible(true);
                }, this);

                return this;
            },

            afterResolveDocument: function () {
                this.saveBillingAddress();

                addressAutoComplete.register('billing');
            },

            /**
             * @return {Boolean}
             */
            useShippingAddress: function () {
                if (this.isAddressSameAsShipping()) {
                    selectBillingAddress(quote.shippingAddress());
                    checkoutData.setSelectedBillingAddress(null);
                    if (window.checkoutConfig.reloadOnBillingAddress) {
                        setBillingAddressAction(globalMessageList);
                    }
                } else {
                    this.updateAddress();
                }

                return true;
            },

            onAddressChange: function (address) {
                this._super(address);

                if (!this.isAddressSameAsShipping() && canShowBillingAddress) {
                    this.updateAddress();
                }
            },

            /**
             * Update address action
             */
            updateAddress: function () {
                if (this.selectedAddress() && !this.isAddressFormVisible()) {
                    newBillingAddress = createBillingAddress(this.selectedAddress());
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(this.selectedAddress().getKey());
                } else {
                    var addressData = this.source.get('billingAddress'),
                        newBillingAddress;

                    if (customer.isLoggedIn() && !this.customerHasAddresses) {
                        this.saveInAddressBook(1);
                    }
                    addressData.save_in_address_book = this.saveInAddressBook() ? 1 : 0;
                    newBillingAddress = createBillingAddress(addressData);

                    // New address must be selected as a billing address
                    selectBillingAddress(newBillingAddress);
                    checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
                    checkoutData.setNewCustomerBillingAddress(addressData);
                }
                if (window.checkoutConfig.reloadOnBillingAddress) {
                    setBillingAddressAction(globalMessageList);
                }                
            },

            /**
             * Perform postponed binding for fieldset elements
             */
            initFields: function () {
                var self = this,
                    addressFields = window.checkoutConfig.oscConfig.addressFields,
                    fieldsetName = 'checkout.steps.shipping-step.billingAddress.billing-address-fieldset';

                // var elements = registry.async(fieldsetName)().elems();

                $.each(addressFields, function (index, field) {
                    registry.async(fieldsetName + '.' + field)(self.bindHandler.bind(self));
                });

                return this;
            },

            initSameStudent: function () {
                this.isAddressSameAsStudent.subscribe(function(newValue) {
                    if(newValue) {
                        $('#checkout-step-billing #opc-billing-form input, #checkout-step-billing #opc-billing-form select').attr('disabled','disabled');

                        var studentAddressData = checkoutData.getStudentAddressFromData();
                        if(studentAddressData) {
                            checkoutData.setInputFieldEmailValue(studentAddressData.email);
                            // if(customer.isLoggedIn()) {
                            //     quote.billingAddress.isAddressFormVisible(true);
                            //     checkout.setSelectedBillingAddress(null);
                            // }
                            quote.guestEmail = studentAddressData.email;
                            // checkoutData.setValidatedEmailValue(studentAddressData.email);
                            checkoutData.setBillingAddressFromData(studentAddressData);

                            $('#checkout-step-billing #customer-email').val(studentAddressData.email);

                            registry.async('checkoutProvider')(function (checkoutProvider) {
                                var billingAddressData = checkoutData.getBillingAddressFromData();

                                if (billingAddressData) {
                                    checkoutProvider.set(
                                        'billingAddress',
                                        $.extend({}, checkoutProvider.get('billingAddress'), billingAddressData)
                                    );
                                };

                            });
                        }

                    }else {
                        $('#checkout-step-billing input, #checkout-step-billing select').removeAttr('disabled','disabled');
                    }
                });
            },

            bindHandler: function (element) {
                var self = this;

                if (element.component.indexOf('/group') !== -1) {
                    $.each(element.elems(), function (index, elem) {
                        self.bindHandler(elem);
                    });
                } else {
                    element.on('value', this.saveBillingAddress.bind(this, element.index));
                    observedElements.push(element);
                }
            },

            saveBillingAddress: function (fieldName) {
                if (!this.isAddressSameAsShipping()) {
                    if (!canShowBillingAddress) {
                        selectBillingAddress(quote.shippingAddress());
                    } else {
                        var addressFlat = addressConverter.formDataProviderToFlatData(
                            this.collectObservedData(),
                            'billingAddress'
                        );

                        selectBillingAddress(addressConverter.formAddressDataToQuoteAddress(addressFlat));
                        if (window.checkoutConfig.reloadOnBillingAddress && (fieldName == 'country_id')) {
                            setBillingAddressAction(globalMessageList);
                         }
                    }
                }
            },

            /**
             * Collect observed fields data to object
             *
             * @returns {*}
             */
            collectObservedData: function () {
                var observedValues = {};

                $.each(observedElements, function (index, field) {
                    observedValues[field.dataScope] = field.value();
                });

                return observedValues;
            },

            validate: function () {
                if (this.isAddressSameAsStudent()) {
                    var studentAddressData = checkoutData.getStudentAddressFromData();
                    if(studentAddressData) {
                        checkoutData.setInputFieldEmailValue(studentAddressData.email);
                        // if(customer.isLoggedIn()) {
                        //     quote.billingAddress.isAddressFormVisible(true);
                        //     checkout.setSelectedBillingAddress(null);
                        // }
                        quote.guestEmail = studentAddressData.email;
                        // checkoutData.setValidatedEmailValue(studentAddressData.email);
                        checkoutData.setBillingAddressFromData(studentAddressData);

                        registry.async('checkoutProvider')(function (checkoutProvider) {
                            var billingAddressData = checkoutData.getBillingAddressFromData();

                            if (billingAddressData) {
                                checkoutProvider.set(
                                    'billingAddress',
                                    $.extend({}, checkoutProvider.get('billingAddress'), billingAddressData)
                                );
                            };

                        });
                    }
                    return true;
                }

                if (this.isAddressSameAsShipping()) {
                    oscData.setData('same_as_shipping', true);
                    return true;
                }

                if (!this.isAddressFormVisible()) {
                    return true;
                }

                this.source.set('params.invalid', false);
                this.source.trigger('billingAddress.data.validate');

                if (this.source.get('billingAddress.custom_attributes')) {
                    this.source.trigger('billingAddress.custom_attributes.data.validate');
                }

                oscData.setData('same_as_shipping', false);
                return !this.source.get('params.invalid');
            },
            getAddressTemplate: function () {
                return 'Mageplaza_Osc/container/address/billing-address';
            }
        });
    }
);
