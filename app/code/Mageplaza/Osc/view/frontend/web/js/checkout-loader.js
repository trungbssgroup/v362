/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'rjsResolver'
], function (resolver) {
    'use strict';

    /**
     * Removes provided loader element from DOM.
     *
     * @param {HTMLElement} $loader - Loader DOM element.
     */
    function hideLoader($loader) {
        if ((jQuery("#paypal_express").length > 0)){ 
            //jQuery("#paypal_express").parent().parent().hide();
            if(jQuery("#paypal_express").parent().parent().hasClass('_active')){
                jQuery('.custom-paypal').addClass('active');
            }
            jQuery('.custom-paypal').show(); 
        }else{
            jQuery('.custom-paypal').hide(); 
        };
        jQuery('.custom-paypal').click(function(){
            if(jQuery(this).hasClass('active')){

            }else{
                jQuery('#paypal_express').trigger("click");
                jQuery(this).addClass('active');
            }
            jQuery('#co-place-order-area button').trigger("click");
            
        });
        jQuery('.payment-method').click(function(){
            jQuery('.custom-paypal').removeClass('active');
        })
        $loader.parentNode.removeChild($loader);
    }

    /**
     * Initializes assets loading process listener.
     *
     * @param {Object} config - Optional configuration
     * @param {HTMLElement} $loader - Loader DOM element.
     */
    function init(config, $loader) {
        resolver(hideLoader.bind(null, $loader));
    }
    return init;
});
