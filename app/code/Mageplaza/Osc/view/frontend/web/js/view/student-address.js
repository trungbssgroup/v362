define(
    [
        'ko',
        'jquery',
        'Magento_Ui/js/form/form',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/action/set-shipping-information',
        'Mageplaza_Osc/js/action/payment-total-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Magento_Checkout/js/model/shipping-service',
        'Mageplaza_Osc/js/model/checkout-data-resolver',
        'Mageplaza_Osc/js/model/address/auto-complete',
        'rjsResolver',
        'Magento_Checkout/js/model/url-builder',
        'uiRegistry'
    ],
    function (ko,
              $,
              Component,
              quote,
              customer,
              setShippingInformationAction,
              getPaymentTotalInformation,
              stepNavigator,
              additionalValidators,
              checkoutData,
              selectBillingAddress,
              selectShippingAddress,
              addressConverter,
              shippingRateService,
              shippingService,
              oscDataResolver,
              addressAutoComplete,
              resolver,
              urlBuilder,
              registry) {
        'use strict';

        var observedElements = [];
        // var countryData = customerData.get('directory-data');
        /**
        *
        * mystep - is the name of the component's .html template, 
        * <Vendor>_<Module>  - is the name of the your module directory.
        * 
        */
        return Component.extend({
            defaults: {
                template: ''
            },
 
            //add here your logic to display step,
            isVisible: ko.observable(true),
 
            /**
            *
            * @returns {*}
            */
            initialize: function () {
                this._super();

                // this.initFields();
                additionalValidators.registerValidator(this);

                // register your step
                stepNavigator.registerStep(
                    //step code will be used as step content id in the component template
                    'step_code',
                    //step alias
                    null,
                    //step title value
                    'Step Title',
                    //observable property with logic when display step or hide step
                    this.isVisible,
                     
                    _.bind(this.navigate, this),
 
                    /**
                    * sort order value
                    * 'sort order value' < 10: step displays before shipping step;
                    * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                    * 'sort order value' > 20 : step displays after payment step
                    */
                    15
                );

                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var studentAddressData = checkoutData.getStudentAddressFromData();

                    if (studentAddressData) {
                        checkoutProvider.set(
                            'studentAddress',
                            $.extend({}, checkoutProvider.get('studentAddress'), studentAddressData)
                        );
                    }
                    checkoutProvider.on('studentAddress', function (studentAddressData) {
                        checkoutData.setStudentAddressFromData(studentAddressData);
                    });
                });

                // resolver(this.afterResolveDocument.bind(this));
 
                return this;
            },

            initObservable: function () {
                this._super();

                // quote.shippingMethod.subscribe(function (oldValue) {
                //     this.currentMethod = oldValue;
                // }, this, 'beforeChange');

                // quote.shippingMethod.subscribe(function (newValue) {
                //     var isMethodChange = ($.type(this.currentMethod) !== 'object') ? true : this.currentMethod.method_code;
                //     if ($.type(newValue) === 'object' && (isMethodChange != newValue.method_code)) {
                //         setShippingInformationAction();
                //     } else if (shippingRateService.isAddressChange) {
                //         shippingRateService.isAddressChange = false;
                //         getPaymentTotalInformation();
                //     }
                // }, this);

                return this;
            },

            afterResolveDocument: function () {
                addressAutoComplete.register('student');
            },

            /**
             * Perform postponed binding for fieldset elements
             */
            // initFields: function () {
            //     var self = this,
            //         fieldsetName = 'checkout.steps.shipping-step.studentAddress.custom-checkout-form-fieldset';

            //     var elements = registry.async(fieldsetName)().elems();

            //     $.each(elements, function (index, elem) {
            //         self.bindHandler(elem);
            //     });

            //     return this;
            // },

            // bindHandler: function (element) {
            //     var self = this;

            //     if (element.component.indexOf('/group') !== -1) {
            //         $.each(element.elems(), function (index, elem) {
            //             self.bindHandler(elem);
            //         });
            //     } else {
            //         // element.on('value', this.saveStudentAddress.bind(this));
            //         observedElements.push(element);
            //     }
            // },
            validate: function () {
                // if (quote.isVirtual()) {
                //     return true;
                // }

                // var shippingMethodValidationResult = true,
                    var studentAddressValidationResult = true;
                    // loginFormSelector = 'form[data-role=email-with-possible-login]',
                    // emailValidationResult = customer.isLoggedIn();

                // if (!quote.shippingMethod()) {
                //     this.errorValidationMessage('Please specify a shipping method.');

                //     shippingMethodValidationResult = false;
                // }

                // if (!customer.isLoggedIn()) {
                //     $(loginFormSelector).validation();
                //     emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                // }

                if (this.isFormInline) {
                    this.source.set('params.invalid', false);
                    this.source.trigger('studentAddress.data.validate');

                    if (this.source.get('studentAddress.custom_attributes')) {
                        this.source.trigger('studentAddress.custom_attributes.data.validate');
                    }

                    if (this.source.get('params.invalid')) {
                        studentAddressValidationResult = false;
                    }

                    // this.saveStudentAddress();
                }

                // console.log(studentAddressValidationResult);
                // if(studentAddressValidationResult) {
                    
                    
                // }else {
                    return studentAddressValidationResult;
                // }

                // if (!emailValidationResult) {
                //     $(loginFormSelector + ' input[name=username]').focus();
                // }

                
            },

            // saveStudentAddress: function () {
            //     var studentAddress = quote.studentAddress(),
            //         addressData = addressConverter.formAddressDataToQuoteAddress(
            //             this.source.get('studentAddress')
            //         );

            //     // console.log(addressData);
            //     studentAddress = addressData;
            //     quote.studentAddress(studentAddress);

            //     //Copy form data to quote shipping address object
            //     // for (var field in addressData) {
            //     //     if (addressData.hasOwnProperty(field) &&
            //     //         studentAddress.hasOwnProperty(field) &&
            //     //         typeof addressData[field] != 'function' &&
            //     //         _.isEqual(studentAddress[field], addressData[field])
            //     //     ) {
            //     //         studentAddress[field] = addressData[field];
            //     //     } else if (typeof addressData[field] != 'function' && !_.isEqual(studentAddress[field], addressData[field])) {
            //     //         studentAddress = addressData;
            //     //         break;
            //     //     }
            //     // }

                
            //     // selectShippingAddress(shippingAddress);
            // },
 
            /**
            * The navigate() method is responsible for navigation between checkout step
            * during checkout. You can add custom logic, for example some conditions
            * for switching to your custom step 
            */
            navigate: function () {
 
            },
 
            /**
            * @returns void
            */
            navigateToNextStep: function () {
                stepNavigator.next();
            },
            getAddressTemplate: function () {
                return 'Mageplaza_Osc/container/address/student-address';
            },
            /**
             * @param {int} countryId
             * @return {*}
             */
            getCountryName: function (countryId) {
                return countryData()[countryId] != undefined ? countryData()[countryId].name : '';
            }
        });
    }
);