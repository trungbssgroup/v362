<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\Osc\Api\Data;

/**
 * Interface AddressInterface
 * @api
 */
interface AddressInterface extends \Magento\Quote\Api\Data\AddressInterface
{
	const KEY_SUITE = 'suite';

	/**
     * Get suite name
     *
     * @return string
     */
    public function getSuite();

    /**
     * Set suite name
     *
     * @param string $region
     * @return $this
     */
    public function setSuite($suite);
}