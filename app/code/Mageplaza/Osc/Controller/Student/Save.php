<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Osc
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Mageplaza\Osc\Controller\Student;

/**
 * Class Index
 * @package Mageplaza\Osc\Controller\Index
 */
class Save extends \Magento\Framework\App\Action\Action
{
	/**
	 * @type \Mageplaza\Osc\Helper\Data
	 */
	protected $_request;

	/**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote = null;

	public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\RequestInterface $request
    )
    {
    	$this->_request = $request;
    	$this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

	/**
	 *
	 * @return $this|\Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$data = $this->_request->getParams();
		$address = $this->getQuote()->getStudentAddress();
		$id = $address->getId();

		$address->setData($data);
		$address->setAddressType('student');
		$address->setId($id)->save();
		echo 'done';

		// $this->_checkoutHelper = $this->_objectManager->get('Mageplaza\Osc\Helper\Data');
		// if (!$this->_checkoutHelper->isEnabled()) {
		// 	$this->messageManager->addError(__('One step checkout is turned off.'));

		// 	return $this->resultRedirectFactory->create()->setPath('checkout');
		// }
	}

	public function getQuote()
    {
        if ($this->_quote === null) {
            return $this->_checkoutSession->getQuote();
        }
        return $this->_quote;
    }
}
