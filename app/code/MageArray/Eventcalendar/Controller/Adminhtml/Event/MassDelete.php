<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class MassDelete extends Action
{
    /**
     * @return $this
     */
    public function execute()
    {

        $eventsIds = $this->getRequest()->getParam('event');
        if (!is_array($eventsIds) || empty($eventsIds)) {
            $this->messageManager->addError(__('Please select Event(s).'));
        } else {
            try {
                foreach ($eventsIds as $postId) {
                    $post = $this->_objectManager
                        ->get('MageArray\Eventcalendar\Model\Event')
                        ->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.',
                        count($eventsIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory
            ->create()->setPath('eventcalendar/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::manageevent');
    }
}
