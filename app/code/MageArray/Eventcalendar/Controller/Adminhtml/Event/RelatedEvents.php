<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use MageArray\Eventcalendar\Controller\Adminhtml\Event;

/**
 * Class RelatedEvents
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class RelatedEvents extends Event
{
    /**
     *
     */
    public function execute()
    {
        if (empty($this->_model)) {
            $this->_model = $this->_objectManager
                ->create('MageArray\Eventcalendar\Model\Event');

            $id = (int)$this->getRequest()->getParam('event_id');
            if ($id) {
                $this->_model->load($id);
            }
        }
        $this->_coreRegistry->register('related_event_model', $this->_model);
        $this->_view->loadLayout()
            ->getLayout()
            ->getBlock('magearray_eventcalendar_relatedevents')
            ->setEventsRelated(
                $this->getRequest()->getPost('events_related', null)
            );
        $this->_view->renderLayout();
    }
}
