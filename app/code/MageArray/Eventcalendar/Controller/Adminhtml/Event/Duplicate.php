<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use MageArray\Eventcalendar\Controller\Adminhtml\Event;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;

/**
 * Class Duplicate
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class Duplicate extends Event
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $eventId = $this->getRequest()->getParam('event_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_eventFactory->create();
        if ($eventId) {
            $model->load($eventId);
        }
        try {
            $newModel = clone $model;
            $newModel->setEventId(null);
            $newModel->isObjectNew(true);
            $newModel->setData('status', 'disable');
            $newModel->setUrlKey(null);
            $newModel->getResource()->save($newModel);
            $relatedTable = $this->_resource->getTableName('magearray_eventcalendar_event_relatedevent');
            $query = 'select * from ' . $relatedTable . ' where event_id="' . $eventId . '"';
            $query = $this->_resource->getConnection()->query($query);
            $result = $query->fetchAll();
            foreach ($result as $relatedId) {
                $query = 'INSERT INTO ' . $relatedTable . ' 
                                    (event_id,related_id,position) 
                                    VALUES (' . $newModel->getEventId() . ',' . $relatedId['related_id'] . ',
                                    ' . $relatedId['position'] . ')';
                $this->_resource->getConnection()->query($query);
            }
            $this->messageManager->addSuccessMessage(__('You duplicated the Event.'));
            $resultRedirect->setPath('eventcalendar/event/edit',
                ['_current' => true, 'event_id' => $newModel->getEventId()]);
        } catch (\Exception $e) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/DuplicateEvent.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($e);
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect->setPath('eventcalendar/event/edit',
                ['_current' => true]);
        }
        return $resultRedirect;
    }
}
