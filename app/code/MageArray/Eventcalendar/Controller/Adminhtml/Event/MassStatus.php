<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use Magento\Backend\App\Action;

/**
 * Class MassStatus
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class MassStatus extends Action
{
    /**
     * @return $this
     */
    public function execute()
    {
        $eventsIds = $this->getRequest()->getParam('event');
        if (!is_array($eventsIds) || empty($eventsIds)) {
            $this->messageManager->addError(__('Please select Events(s).'));
        } else {
            try {
                $status = $this->getRequest()->getParam('status');
                foreach ($eventsIds as $postId) {
                    $statuses = $this->_objectManager
                        ->get('MageArray\Eventcalendar\Model\Event')
                        ->load($postId);
                    $statuses->setStatus($status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.',
                        count($eventsIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory
            ->create()->setPath('eventcalendar/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::manageevent');
    }
}
