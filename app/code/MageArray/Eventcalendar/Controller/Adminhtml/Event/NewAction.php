<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use MageArray\Eventcalendar\Controller\Adminhtml\Event;

/**
 * Class NewAction
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class NewAction extends Event
{
    /**
     *
     */
    public function execute()
    {
        $model = $this->_eventFactory->create();
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('event_post', $model);
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
