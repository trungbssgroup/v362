<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
class Importsave extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
	
    protected $_storeManager;
	
    public function __construct(
        Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
    ) {
        $this->_storeManager = $storeManager;
		$this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
		$resultRedirect = $this->resultRedirectFactory->create();
		$uploader = $this->_fileUploaderFactory->create(['fileId' => 'uploadFile']);
		$uploader->setAllowedExtensions(['csv']);
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $mediaDirectory = $this->_objectManager
            ->get('Magento\Framework\Filesystem')
            ->getDirectoryRead(DirectoryList::MEDIA);
        $result = $uploader->save($mediaDirectory
            ->getAbsolutePath('eventsCsv'));
        $csvFile = $result['path'] . '/' . $result['file'];  
		if (is_file($csvFile)) {
			$input = fopen($csvFile, 'a+');
			$header = null;
			while (($row = fgetcsv($input, 1024, ',')) !== false) {
				if (!$header) {
					$header = $row;
				} else {
					$check = $this->is_array_empty($row);
					if (!$check) {
						$data[] = array_combine($header, $row);
					}
				}
			}
			unset($data['form_key']);

			if (count($data) > 0) {
				$errorFiles = array();
				$i = 0;
				foreach ($data as $dtkey => $datval) {
					$model = $this->_objectManager->create('MageArray\Eventcalendar\Model\Event');

					$eventCollection = $model->getCollection()
						->addFieldToFilter('url_key',
							trim($datval['url_key']));
					if ($eventCollection->getSize() > 0) {
						$errorFiles[] = $datval['url_key'];
					} else {
						foreach ($datval as $key => $val) {
							$model->setData($key, $val);
						}
					}
					try {
						if ($model->getData()) {
							$model->save();
							$i++;
						}
					} catch (\Magento\Framework\Exception\LocalizedException $e) {
						$this->messageManager->addError($e->getMessage());
						return $resultRedirect->setPath('*/*/');
					} catch (\RuntimeException $e) {
						$this->messageManager->addError($e->getMessage());
						return $resultRedirect->setPath('*/*/');
					} catch (\Exception $e) {
						$this->messageManager->addException($e,
							__('Something went wrong while saving the Events.'));
						return $resultRedirect->setPath('*/*/');
					}
				}

				$this->messageManager->addSuccess(__('%1 Events imported successfully.',
					$i));
				if (count($errorFiles)) {
					$this->messageManager->addError(__("Some error's on uploading this Evnet's url Same %1",
						implode(', ', $errorFiles)));
				}
			} else {
				$this->messageManager->addError(__('Some Error occur in import , Please try again'));
			}

			return $resultRedirect->setPath('*/*/');
		}
    }

    public function is_array_empty($a)
    {
        foreach ($a as $elm) {
            if (!empty($elm)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::importevents');
    }

}
