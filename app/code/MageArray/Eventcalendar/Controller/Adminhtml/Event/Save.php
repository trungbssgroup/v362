<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use MageArray\Eventcalendar\Controller\Adminhtml\Event;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;

/**
 * Class Save
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class Save extends Event
{

    /**
     *
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
			$store = implode(",", $data['stores']);
			if (isset($data['categories'])) {
				$data['categories'] = implode(",", $data['categories']);
			}
            $model = $this->_objectManager
                ->create('MageArray\Eventcalendar\Model\Event');
            $id = $this->getRequest()->getParam('event_id');
			if ($id) {
                $model->load($id);
            }
			if($data['url_key']){
				$collection = $model->getCollection()
                ->addFieldToFilter('url_key', trim($data['url_key']))
                ->addFieldToFilter('event_id', array('nin' => array('finset' => $model->getId())));
				if($collection->getSize() > 0){
					$this->messageManager->addError(__("Same Url Key %1 in Event ",$data['url_key']));
					$this->_redirect('*/*/edit',
                        ['event_id' => $model->getId(), '_current' => true]);
						return;
				}
			}
            $mediaDirectory = $this->_objectManager
                ->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);
            if (isset($data['event_image'])) {
                $imageData = $data['event_image'];
                if (isset($imageData['delete'])) {
                    $io = $this->_ioFile;
                    $io->open(['path' => $mediaDirectory->getAbsolutePath('event/')]);
                    if ($io->fileExists($imageData['value'])) {
                        $io->rm($imageData['value']);
                        $data['event_image'] = '';
                    }
                }
            }

            try {
                $uploader = $this->uploaderFactory->create(['fileId' => 'event_image']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->setAllowCreateFolders(true);

                $result = $uploader->save($mediaDirectory
                    ->getAbsolutePath('event/'));
                if ($result['error'] == 0) {
                    $data['event_image'] = $result['file'];
                }
            } catch (\Exception $e) {
                if ($e->getCode() != Uploader::TMP_NAME_EMPTY) {
                    throw new \Magento\Framework\Validator\Exception($e->getMessage());
                } else {
                    if (isset($data['event_image']['value'])) {
                        $data['event_image'] = $data['event_image']['value'];
                    }
                }
            }
            
            try {
                $address = preg_replace('/\s+/', ' ', $data['address']);
                $model->setData($data);
                $model->setData('store_id', $store);
                $model->setData('address', $address);
                $model->save();
                $eventId = $model->getId();
                if (isset($data['links'])) {
                    $links = $data['links'];
                    $jsHelper = $this->_objectManager
                        ->create('Magento\Backend\Helper\Js');
                    $links = is_array($links) ? $links : [];
                    $linkTypes = ['relatedevents'];
                    foreach ($linkTypes as $type) {
                        if (isset($links[$type])) {
                            $links[$type] = $jsHelper
                                ->decodeGridSerializedInput($links[$type]);
                            if ($type == 'relatedevents') {
                                $relatedEvent = $links[$type];
                            }
                        }
                    }

                    $eventmanager = $this->_objectManager
                        ->get('MageArray\Eventcalendar\Model\ResourceModel\Event');
                    if (isset($relatedEvent)) {
                        $newRelatedIds = $relatedEvent;
                        if (is_array($newRelatedIds)) {
                            $oldRelatedIds = $eventmanager
                                ->getRelatedEventIds($eventId);
                            $insert = array_keys($newRelatedIds);
                            $delete = $oldRelatedIds;

                            $relatedTable = $this->_resource
                                ->getTableName(
                                    'magearray_eventcalendar_event_relatedevent'
                                );
                            if ($delete) {
                                foreach ($delete as $deleteId) {
                                    $query = 'DELETE FROM ' . $relatedTable . ' WHERE 
                                    event_id="' . $eventId . '" and
                                     related_id = "' . $deleteId . '"';
                                    $this->_resource->getConnection()->query($query);
                                }
                            }

                            if ($insert) {
                                $data = [];
                                foreach ($insert as $relatedId) {
                                    if (isset($relatedEvent[$relatedId])
                                        && is_array($relatedEvent[$relatedId])
                                    ) {
                                        $position = $relatedEvent[$relatedId]['position'];
                                    } else {
                                        $position = 0;
                                    }
                                    if (!isset($position) || $position == "") {
                                        $position = 0;
                                    }
                                    $query = 'INSERT INTO ' . $relatedTable . ' 
                                    (event_id,related_id,position) 
                                    VALUES (' . $eventId . ',' . $relatedId . ',
                                    ' . $position . ')';
                                    $this->_resource->getConnection()->query($query);
                                }
                            }
                        }
                    }
                }
                $this->messageManager
                    ->addSuccess(__('Event has been saved.'));
                $this->_objectManager
                    ->get('Magento\Backend\Model\Session')
                    ->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit',
                        ['event_id' => $model->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager
                    ->addException($e,
                        __('Something went wrong while saving Event.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit',
                ['event_id' => $this->getRequest()->getParam('event_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }
}
