<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Event;

use MageArray\Eventcalendar\Controller\Adminhtml\Event;

/**
 * Class Edit
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Event
 */
class Edit extends Event
{

    /**
     *
     */
    public function execute()
    {

        $id = $this->getRequest()->getParam('event_id');
        $model = $this->_eventFactory->create();

        if ($id) {
            $model->load($id);
            if (!$model->getEventId()) {
                $this->messageManager
                    ->addError(__('This Event no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
            $address = preg_replace('/\s+/', ' ', $data['address']);
            $model->setData('address', $address);
        }
        $this->_coreRegistry->register('event_post', $model);
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
