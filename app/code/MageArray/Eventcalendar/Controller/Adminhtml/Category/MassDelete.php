<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Category
 */
class MassDelete extends Action
{
    /**
     * @return $this
     */
    public function execute()
    {
        $categoriesIds = $this->getRequest()->getParam('category');
        if (!is_array($categoriesIds) || empty($categoriesIds)) {
            $this->messageManager->addError(__('Please select Category(s).'));
        } else {
            try {
                foreach ($categoriesIds as $postId) {
                    $post = $this->_objectManager
                        ->get('MageArray\Eventcalendar\Model\Category')
                        ->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.',
                        count($categoriesIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory
            ->create()->setPath('eventcalendar/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::managecategory');
    }
}
