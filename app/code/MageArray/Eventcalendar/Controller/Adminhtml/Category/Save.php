<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Category;

use MageArray\Eventcalendar\Controller\Adminhtml\Category;

/**
 * Class Save
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Category
 */
class Save extends Category
{

    /**
     *
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (isset($data['stores']) && !empty($data['stores'])) {
            $store = implode(",", $data['stores']);
            $data['stores'] = $store;
        }
        if ($data) {
            $model = $this->_objectManager
                ->create('MageArray\Eventcalendar\Model\Category');
            $id = $this->getRequest()->getParam('category_id');
            if ($id) {
                $model->load($id);
            }
            try {
                $model->setData($data);
                $model->save();
                $this->messageManager
                    ->addSuccess(__('Category has been saved.'));
                $this->_objectManager
                    ->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit',
                        ['category_id' => $model->getId(), '_current' => true]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager
                    ->addException($e,
                        __('Something went wrong while saving Category.'));
            }
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit',
                ['category_id' => $this->getRequest()->getParam('category_id')]);
            return;
        }
        $this->_redirect('*/*/');
    }
}
