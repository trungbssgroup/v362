<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

/**
 * Class Delete
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Category
 */
class Delete extends Action
{
    /**
     * @return $this
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('category_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager
                    ->create('MageArray\Eventcalendar\Model\Category');
                $model->load($id);
                $model->delete();
                $this->messageManager
                    ->addSuccess(__('Category has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager
                    ->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit',
                    ['category_id' => $id]);
            }
        }
        $this->messageManager
            ->addError(__('We can\'t find a Category to delete.'));
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::managecategory');
    }
}
