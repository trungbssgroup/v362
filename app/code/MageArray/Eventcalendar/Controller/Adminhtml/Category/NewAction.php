<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml\Category;

use MageArray\Eventcalendar\Controller\Adminhtml\Category;

/**
 * Class NewAction
 * @package MageArray\Eventcalendar\Controller\Adminhtml\Category
 */
class NewAction extends Category
{
    /**
     *
     */
    public function execute()
    {
        $model = $this->_categoryFactory->create();
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('category_post', $model);
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
