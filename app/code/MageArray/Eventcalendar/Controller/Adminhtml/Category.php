<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml;

use MageArray\Eventcalendar\Model\CategoryFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Registry;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Category
 * @package MageArray\Eventcalendar\Controller\Adminhtml
 */
abstract class Category extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var Session
     */
    protected $_authSession;
    /**
     * @var Escaper
     */
    protected $_escaper;
    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * Category constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param Session $authSession
     * @param Registry $coreRegistry
     * @param Escaper $escaper
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        Session $authSession,
        Registry $coreRegistry,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_authSession = $authSession;
        $this->_categoryFactory = $categoryFactory;
        $this->_escaper = $escaper;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::managecategory');
    }
}
