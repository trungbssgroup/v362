<?php
namespace MageArray\Eventcalendar\Controller\Adminhtml;

use MageArray\Eventcalendar\Model\EventFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Registry;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\App\ResourceConnection;

/**
 * Class Event
 * @package MageArray\Eventcalendar\Controller\Adminhtml
 */
abstract class Event extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var FileFactory
     */
    protected $_fileFactory;
    /**
     * @var EventFactory
     */
    protected $_eventFactory;
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var Session
     */
    protected $_authSession;
    /**
     * @var Escaper
     */
    protected $_escaper;
    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var StateInterface
     */
    protected $inlineTranslation;
    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $_ioFile;
    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;
    /**
     * @var ResourceConnection
     */
    protected $_resource;

    /**
     * Event constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param Session $authSession
     * @param Registry $coreRegistry
     * @param Escaper $escaper
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param FileFactory $fileFactory
     * @param PageFactory $resultPageFactory
     * @param UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param ResourceConnection $resource
     */
    public function __construct(
        Context $context,
        EventFactory $eventFactory,
        Session $authSession,
        Registry $coreRegistry,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        FileFactory $fileFactory,
        PageFactory $resultPageFactory,
        UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem\Io\File $ioFile,
        ResourceConnection $resource
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_authSession = $authSession;
        $this->_eventFactory = $eventFactory;
        $this->_escaper = $escaper;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->resultPageFactory = $resultPageFactory;
        $this->_fileFactory = $fileFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->_ioFile = $ioFile;
        $this->_resource = $resource;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('MageArray_Eventcalendar::manageevent');
    }
}
