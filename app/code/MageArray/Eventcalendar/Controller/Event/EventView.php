<?php
namespace MageArray\Eventcalendar\Controller\Event;

use MageArray\Eventcalendar\Model\EventFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;


/**
 * Class EventView
 * @package MageArray\Eventcalendar\Controller\Event
 */
class EventView extends Action
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var EventFactory
     */
    protected $_eventFactory;

    /**
     * EventView constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param EventFactory $eventFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        EventFactory $eventFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_eventFactory = $eventFactory;
        parent::__construct($context);
    }

    /**
     *
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $eventObject = $this->_eventFactory->create()->load($id);
            $this->_coreRegistry->register('event', $eventObject);
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        }
    }
}
