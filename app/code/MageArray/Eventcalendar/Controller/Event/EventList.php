<?php
namespace MageArray\Eventcalendar\Controller\Event;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class EventList
 * @package MageArray\Eventcalendar\Controller\Event
 */
class EventList extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * EventList constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}

