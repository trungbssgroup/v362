<?php
namespace MageArray\Eventcalendar\Controller;

use MageArray\Eventcalendar\Helper\Data;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\UrlInterface;
use MageArray\Eventcalendar\Model\EventFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Url;

/**
 * Class Router
 * @package MageArray\Eventcalendar\Controller
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;
    /**
     * @var
     */
    protected $_response;
    /**
     * @var
     */
    protected $dispatched;
    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var ManagerInterface
     */
    protected $eventManager;
    /**
     * @var UrlInterface
     */
    protected $url;
    /**
     * @var EventFactory
     */
    protected $eventFactory;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Router constructor.
     * @param ActionFactory $actionFactory
     * @param ManagerInterface $eventManager
     * @param UrlInterface $url
     * @param EventFactory $eventFactory
     * @param StoreManagerInterface $storeManager
     * @param ResponseInterface $response
     * @param Data $dataHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        ActionFactory $actionFactory,
        ManagerInterface $eventManager,
        UrlInterface $url,
        EventFactory $eventFactory,
        StoreManagerInterface $storeManager,
        ResponseInterface $response,
        Data $dataHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {

        $this->actionFactory = $actionFactory;
        $this->eventManager = $eventManager;
        $this->url = $url;
        $this->eventFactory = $eventFactory;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->response = $response;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        if (!$this->dispatched) {
            $urlKey = trim($request->getPathInfo(), '/');
            $condition = new DataObject(
                ['url_key' => $urlKey, 'continue' => true]
            );
            $this->eventManager->dispatch(
                'magearray_eventcalendar_controller_router_match_before',
                ['router' => $this, 'condition' => $condition]
            );
            $urlKey = $condition->getUrlKey();

            if ($condition->getRedirectUrl()) {
                $this->response->setRedirect($condition->getRedirectUrl());
                $request->setDispatched(true);
                return $this->actionFactory->create(
                    'Magento\Framework\App\Action\Redirect',
                    ['request' => $request]
                );
            }
            if (!$condition->getContinue()) {
                return null;
            }
            $entities = [
                'author' => [
                    'list_key' => $this->_dataHelper->getPageUrl(),
                    'list_action' => 'eventlist',
                    'controller' => 'event',
                    'action' => 'eventlist',
                    'param' => 'id',
                    'factory' => $this->eventFactory,
                ]
            ];

            foreach ($entities as $settings) {
                if ($settings['list_key']) {
                    if ($urlKey == $settings['list_key']) {
                        $request->setModuleName('eventcalendar')
                            ->setControllerName($settings['controller'])
                            ->setActionName($settings['list_action']);
                        $request->setAlias(
                            Url::REWRITE_REQUEST_PATH_ALIAS, $urlKey
                        );
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    }
                }

                $parts = explode('/', $urlKey);
                if ($parts[0] == $settings['list_key']) {
                    if (isset($parts[2]) && isset($parts[3]) && isset($parts[4])) {

                        $controllerName = $parts[1];
                        $actionName = $parts[2];
                        $paramName = $parts[3];
                        $paramValue = $parts[4];

                        $request->setModuleName('eventcalendar')
                            ->setControllerName($controllerName)
                            ->setActionName($actionName)
                            ->setParam($paramName, $paramValue);
                        $request->setDispatched(true);
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    } else {
                        $urlName = $parts[1];
                        $instanceEve = $this->eventFactory->create();
                        $eventId = $instanceEve->checkUrlKey($urlName);

                        if (!$eventId) {
                            return null;
                        }
                        $request->setModuleName('eventcalendar')
                            ->setControllerName('event')
                            ->setActionName('eventview')
                            ->setParam('id', $eventId);
                        $request->setAlias(Url::REWRITE_REQUEST_PATH_ALIAS,
                            $urlName);
                        $request->setDispatched(true);
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    }
                }
            }
        }
        return null;
    }

}