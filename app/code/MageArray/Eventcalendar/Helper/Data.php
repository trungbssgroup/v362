<?php
namespace MageArray\Eventcalendar\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * @package MageArray\Eventcalendar\Helper
 */
class Data extends AbstractHelper
{

    /**
     *
     */
    const XML_PATH_ENABLED = 'eventcalendar/general/enable';
    /**
     *
     */
    const XML_PATH_HEADER_LINK = 'eventcalendar/general/header_link';
    /**
     *
     */
    const XML_PATH_FOOTER_LINK = 'eventcalendar/general/footer_link';
    /**
     *
     */
    const XML_PATH_PAGE_URL = 'eventcalendar/general/page_url';
    /**
     *
     */
    const XML_PATH_LINK_TITLE = 'eventcalendar/general/link_title';
    /**
     *
     */
    const XML_PATH_PAGE_TITLE = 'eventcalendar/general/page_title';
    /**
     *
     */
    const XML_PATH_PAGE_DESCRIPTION = 'eventcalendar/general/page_description';
    /**
     *
     */
    const XML_PATH_META_TITLE = 'eventcalendar/general/meta_title';
    /**
     *
     */
    const XML_PATH_META_KEYWORDS = 'eventcalendar/general/meta_keywords';
    /**
     *
     */
    const XML_PATH_META_DESCRIPTION = 'eventcalendar/general/meta_description';
    /**
     *
     */
    const XML_PATH_EVENT_VIEW_MODE = 'eventcalendar/general/view_mode';

    /**
     *
     */
    const XML_PATH_EXPIRED_EVENT_DISPLAY = 'eventcalendar/general/expired_event_display';
    /**
     *
     */
    const XML_PATH_MAP_API_KEY = 'eventcalendar/general/map_api_key';

    /**
     *
     */
    const XML_PATH_UPCOMING_COUNT = 'eventcalendar/event_display_option/upcoming_count';
    /**
     *
     */
    const XML_PATH_EVENTS_DISPLAY = 'eventcalendar/related_events/events_display';
    /**
     *
     */
    const XML_PATH_EVENTS_COUNT = 'eventcalendar/related_events/event_count';
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;
    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    protected $_imageFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Image\AdapterFactory $imageFactory
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_imageFactory = $imageFactory;
        parent::__construct($context);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ENABLED,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getExpiredEventDisplay($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EXPIRED_EVENT_DISPLAY,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isHeaderLinkEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_HEADER_LINK,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function isFooterLinkEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_FOOTER_LINK,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getPageUrl($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PAGE_URL,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getLinkTitle($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_LINK_TITLE,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getPageTitle($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PAGE_TITLE,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getApiKey($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_MAP_API_KEY,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getPageDescription($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PAGE_DESCRIPTION,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getMetaTitle($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_META_TITLE,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getMetaKeywords($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_META_KEYWORDS,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getMetaDescription($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_META_DESCRIPTION,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getEventViewMode($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EVENT_VIEW_MODE,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getUpcomingPageCount($store = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_UPCOMING_COUNT,
            ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getRelatedEventDisplay($store = null)
    {
        return $this->scopeConfig
            ->getValue(self::XML_PATH_EVENTS_DISPLAY,
                ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getRelatedEventCount($store = null)
    {
        return $this->scopeConfig
            ->getValue(self::XML_PATH_EVENTS_COUNT,
                ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * @param $storePath
     * @return mixed
     */
    public function getStoreConfig($storePath)
    {
        $storeConfig = $this->scopeConfig->getValue($storePath,
            ScopeInterface::SCOPE_STORE);
        return $storeConfig;
    }

    /**
     * @return mixed
     */
    public function getBaseUrlMedia()
    {
        return $this->_storeManager->getStore()
            ->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            );
    }

    /**
     * @param $image
     * @param null $width
     * @param null $height
     * @return string
     */
    public function resize($image, $width = null, $height = null)
    {
        $absolutePath = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('event/') . $image;

        $imageResized = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('resized/' . $width . '/') . $image;
        //create image factory...
        $imageResize = $this->_imageFactory->create();
        $imageResize->open($absolutePath);
        $imageResize->constrainOnly(true);
        $imageResize->keepTransparency(true);
        $imageResize->keepFrame(false);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($width, $height);
        //destination folder                
        $destination = $imageResized;
        //save image      
        $imageResize->save($destination);

        $resizedURL = $this->getBaseUrlMedia() . 'resized/' . $width . '/' . $image;
        return $resizedURL;
    }
}
