<?php
namespace MageArray\Eventcalendar\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package MageArray\Eventcalendar\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->getConnection()->dropColumn($setup->getTable('magearray_eventcalendar_event'),
                'external_url');
            $setup->getConnection()->addColumn($setup->getTable('magearray_eventcalendar_event'),
                    'event_image',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => false,
                        'default' => '',
                        'comment' => 'Event Image'
                    ]
                );
            $setup->getConnection()->addColumn($setup->getTable('magearray_eventcalendar_event'),
                'organizer_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => false,
                    'default' => '',
                    'comment' => 'organizer Name'
                ]
            );
            $setup->endSetup();
            $installer = $setup;
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magearray_eventcalendar_event_relatedevent')
            )->addColumn(
                'event_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'primary' => true],
                'Event ID'
            )->addColumn(
                'related_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'primary' => true],
                'Related Event ID'
            )->addColumn(
                'position',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Position'
            )->addIndex(
                $installer
                    ->getIdxName(
                        'magearray_eventcalendar_event_relatedevent',
                        ['related_id']
                    ),
                ['related_id']
            )->addForeignKey(
                $installer->getFkName(
                    'magearray_eventcalendar_event1',
                    'event_id',
                    'magearray_eventcalendar_event',
                    'event_id'
                ),
                'event_id',
                $installer->getTable('magearray_eventcalendar_event'),
                'event_id',
                Table::ACTION_CASCADE
            )->setComment(
                'Magearray Event to Event Related Relation Table'
            );

            $installer->getConnection()->createTable($table);
        }
    }
}
