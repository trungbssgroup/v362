<?php
namespace MageArray\Eventcalendar\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


/**
 * Class InstallSchema
 * @package MageArray\Eventcalendar\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magearray_eventcalendar_event')
        )->addColumn(
            'event_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Event Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Event Title'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Event Description'
        )->addColumn(
            'external_url',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'External Url'
        )->addColumn(
            'start_date_time',
            Table::TYPE_TIMESTAMP,
            null,
            [],
            'Event Start Date Time'
        )->addColumn(
            'end_date_time',
            Table::TYPE_TIMESTAMP,
            null,
            [],
            'Event End Date Time'
        )->addColumn(
            'status',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Status'
        )->addColumn(
            'color',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Color'
        )->addColumn(
            'venue',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Venue'
        )->addColumn(
            'phone',
            Table::TYPE_TEXT,
            50,
            [],
            'Phone No'
        )->addColumn(
            'fax',
            Table::TYPE_TEXT,
            100,
            [],
            'Fax'
        )->addColumn(
            'email',
            Table::TYPE_TEXT,
            100,
            [],
            'Email'
        )->addColumn(
            'address',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Address'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            10,
            ['nullable' => false],
            'Store ID'
        )->addColumn(
            'url_key',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Url Key'
        )->addColumn(
            'meta_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Meta Title'
        )->addColumn(
            'meta_keywords',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Meta Keywords'
        )->addColumn(
            'meta_description',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Meta Description'
        )->addColumn(
            'ticket_product_sku',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Ticket Product SKU'
        )->addColumn(
            'categories',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Categories'
        );
        $installer->getConnection()->createTable($table);

        $tableTwo = $installer->getConnection()->newTable(
            $installer->getTable('magearray_eventcalendar_category')
        )->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'category Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Event Title'
        )->addColumn(
            'status',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Status'
        )->addColumn(
            'store_id',
            Table::TYPE_INTEGER,
            10,
            ['nullable' => false],
            'Store ID'
        );
        $installer->getConnection()->createTable($tableTwo);
        $installer->endSetup();
    }
}