var config = {
    map: {
        '*': {
            'MageArray_FullCalendar': 'MageArray_Eventcalendar/js/fullcalendar.min',
            'MageArray_Moment': 'MageArray_Eventcalendar/js/moment.min',
            'MageArray_Tooltip': 'MageArray_Eventcalendar/js/jquery.tooltipster',
            'MageArray_Tooltipster': 'MageArray_Eventcalendar/js/jquery.tooltipster.min',
            'carouseljs': 'MageArray_Eventcalendar/js/owl.carousel.min',
        }
    }
};
