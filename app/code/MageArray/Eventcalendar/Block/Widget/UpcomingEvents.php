<?php
namespace MageArray\Eventcalendar\Block\Widget;

use MageArray\Eventcalendar\Model\EventFactory;
use MageArray\Eventcalendar\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

class UpcomingEvents extends Template implements BlockInterface
{

    protected $_template = 'widget/upcoming_event.phtml';
    protected $_eventFactory;
    protected $_dataHelper;

    public function __construct(
        Context $context,
        Data $dataHelper,
        array $data = [],
        EventFactory $eventFactory
    ) {
        $this->_eventFactory = $eventFactory;
        parent::__construct($context, $data);
        $this->_dataHelper = $dataHelper;
        $count = $this->getCount();
        $now = date('Y-m-d H:i:s');
        $collection = $this->_eventFactory->create()->getCollection();
        $collection->addFieldToFilter('status', 'enable');
        $collection->addFieldToFilter('start_date_time', ['gteq' => $now]);
        $collection->setOrder('start_date_time', 'ASC');
        if (isset($count) && $count != '') {
            $collection->setPageSize($count);
        } elseif ($this->_dataHelper->getUpcomingPageCount()) {
            $collection->setPageSize($this->_dataHelper->getUpcomingPageCount());
        } else {
            $collection->setPageSize(5);
        }
        $this->setCollection($collection);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        return $this;
    }

    public function getPageUrl()
    {
        $pageUrl = $this->_dataHelper->getPageUrl();
        return $pageUrl;
    }

    public function getEventUrl($id)
    {
        return $this->getUrl($this->getPageUrl() . '/event/eventview',
            ['id' => $id]);
    }

    public function getPlaceHolderImage()
    {
        return $this->getViewFileUrl(
            'MageArray_Eventcalendar::image/event-placeholder.jpg'
        );
    }
}
