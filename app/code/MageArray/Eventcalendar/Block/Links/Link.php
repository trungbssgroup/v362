<?php
namespace MageArray\Eventcalendar\Block\Links;

use MageArray\Eventcalendar\Helper\Data;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Link
 * @package MageArray\Eventcalendar\Block\Links
 */
class Link extends \Magento\Framework\View\Element\Html\Link
{

    /**
     * @var Data
     */
    protected $_dataHelper;

    /**
     * Link constructor.
     * @param Context $context
     * @param Data $dataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @return string
     */
    public function getHref()
    {
        if ($this->_dataHelper->isEnabled() == 1) {
            $pageUrl = $this->_dataHelper->getPageUrl();
            return $this->getUrl($pageUrl);
        }
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        if ($this->_dataHelper->isEnabled() == 1) {
            return $this->_dataHelper->getLinkTitle();
        }
    }
}
