<?php
namespace MageArray\Eventcalendar\Block\Event;

use MageArray\Eventcalendar\Helper\Data;
use MageArray\Eventcalendar\Model\CategoryFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Cms\Model\Template\FilterProvider;

/**
 * Class EventView
 * @package MageArray\Eventcalendar\Block\Event
 */
class EventView extends Template
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var ProductFactory
     */
    protected $_productFactory;
    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var
     */
    protected $_categoryFactory;
    /**
     * @var
     */
    protected $eventRepository;
    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * EventView constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ProductFactory $productFactory
     * @param Data $dataHelper
     * @param CategoryFactory $categoryFactory
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ProductFactory $productFactory,
        Data $dataHelper,
        CategoryFactory $categoryFactory,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_dataHelper = $dataHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_productFactory = $productFactory;
        $this->_filterProvider = $filterProvider;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {

        if (!$this->_coreRegistry->registry('event') && $this->getEventId()) {
            $event = $this->eventRepository->getById($this->getEventId());
            $this->_coreRegistry->register('event', $event);
        }
        return $this->_coreRegistry->registry('event');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getDescription()
    {
        return $this->_filterProvider
            ->getBlockFilter()->filter($this->getEvent()->getDescription());

    }

    /**
     * @return $this
     */
    public function getTicketProduct()
    {

        $sku = $this->getEvent()->getTicketProductSku();
        if ($sku) {
            $productId = $this->_productFactory->create()->getIdBySku($sku);
            $productData = $this->_productFactory->create()->load($productId);
            return $productData;
        } else {
            return '';
        }
    }

    /**
     * @param $categoryIds
     * @return string
     */
    public function getCategories($categoryIds)
    {
        $categoryIds = explode(',', $categoryIds);
        if ($categoryIds) {
            $cateName = [];
            foreach ($categoryIds as $categoryId) {
                $categorie = $this->_categoryFactory->create()->load($categoryId);
                $cateName[] = $categorie->getTitle();
            }
            return implode(',', $cateName);
        } else {
            return '';
        }
    }

    /**
     * @return array
     */
    public function getLatiudeLongitude()
    {

        $prepAddr = str_replace(' ', ',', $this->getEvent()->getAddress());
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
        $output = json_decode($geocode);
        if (empty($output->results)) {
            $latitude = '';
            $longitude = '';
        } else {
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;
        }
        //return [$latitude, $longitude];
        return ['51.010488', '-113.959808'];

        
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        $url = $this->_dataHelper->getPageUrl();
        return $this->getUrl($url);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->_dataHelper->getApiKey();
    }

    /**
     * @return string
     */
    public function getPlaceHolderImage()
    {
        return $this->getViewFileUrl(
            'MageArray_Eventcalendar::image/event-placeholder.jpg'
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function getEventUrl($id)
    {
        return $this->getUrl($this->getPageUrl() . '/event/eventview',
            ['id' => $id]);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $metaTitle = $this->getEvent()->getMetaTitle();
        $metaKeywords = $this->getEvent()->getMetaKeywords();
        $metaDescription = $this->getEvent()->getMetaDescription();
        $pageTitle = $this->getEvent()->getTitle();
        $pageDescription = $this->getEvent()->getDescription();

        if ($metaTitle) {
            $this->pageConfig->getTitle()->set($metaTitle);
        } else {
            $this->pageConfig->getTitle()->set($pageTitle);
        }

        if ($metaKeywords) {
            $this->pageConfig->setKeywords($metaKeywords);
        } else {
            $this->pageConfig->setKeywords($pageDescription);
        }

        if ($metaDescription) {
            $this->pageConfig->setDescription($metaDescription);
        } else {
            $this->pageConfig->setDescription($pageDescription);
        }

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($pageTitle);
        }
        return parent::_prepareLayout();
    }
}
