<?php
namespace MageArray\Eventcalendar\Block\Event;

use MageArray\Eventcalendar\Helper\Data;
use MageArray\Eventcalendar\Model\Event;
use MageArray\Eventcalendar\Model\EventFactory;
use Magento\Cms\Model\Page;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class RelatedEvents
 * @package MageArray\Eventcalendar\Block\Event
 */
class RelatedEvents extends Template
{

    /**
     * @var
     */
    protected $_eventCollection;
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var Event
     */
    protected $_event;
    /**
     * @var EventFactory
     */
    protected $_eventFactory;
    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * RelatedEvents constructor.
     * @param Context $context
     * @param Event $event
     * @param EventFactory $eventFactory
     * @param Registry $coreRegistry
     * @param Data $dataHelper
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        Event $event,
        EventFactory $eventFactory,
        Registry $coreRegistry,
        Data $dataHelper,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_event = $event;
        $this->_eventFactory = $eventFactory;
        $this->_dataHelper = $dataHelper;
        $this->_coreRegistry = $coreRegistry;
        $this->_filterProvider = $filterProvider;
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $this->_eventCollection = $this->getEvent()->getRelatedEvents()
            ->setPageSize($this->_dataHelper->getRelatedEventCount());
        $this->_eventCollection->getSelect()->order('re.position', 'ASC');
        return $this;
    }

    /**
     * @param $id
     * @return string
     */
    public function getShortDescription($id)
    {
        $eventModel = $this->_eventFactory->create();
        $shortContent = $eventModel->load($id)->getShortContent();
        return $this->_filterProvider->getBlockFilter()->filter($shortContent);
    }

    /**
     * @return mixed
     */
    public function displayEvents()
    {
        return $this->_dataHelper->getRelatedEventDisplay();
    }

    /**
     * @return mixed
     */
    public function getRelatedevents()
    {
        if (empty($this->_eventCollection)) {
            $this->_prepareCollection();
        }
        return $this->_eventCollection;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        if (!$this->hasData('event')) {
            $this->setData('event',
                $this->_coreRegistry->registry('event')
            );
        }
        return $this->getData('event');
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [
            Page::CACHE_TAG . '_relatedevents_' . $this->getEvent()->getId()
        ];
    }

    /**
     * @return string
     */
    public function getPlaceHolderImage()
    {
        return $this->getViewFileUrl(
            'MageArray_Eventcalendar::image/event-placeholder.jpg'
        );
    }

    /**
     * @return mixed
     */
    public function getPageUrl()
    {
        $pageUrl = $this->_dataHelper->getPageUrl();
        return $pageUrl;
    }

    /**
     * @param $id
     * @return string
     */
    public function getEventUrl($id)
    {
        return $this->getUrl($this->getPageUrl() . '/event/eventview',
            ['id' => $id]);
    }
}
