<?php
namespace MageArray\Eventcalendar\Block\Event;

use MageArray\Eventcalendar\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class EventDescription
 * @package MageArray\Eventcalendar\Block\Event
 */
class EventDescription extends Template
{
    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * EventDescription constructor.
     * @param Context $context
     * @param Data $dataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $dataHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return mixed
     */
    public function getPageDescription()
    {
        return $this->dataHelper->getPageDescription();
    }

    public function getPageTitle()
    {
        $pageTitle = $this->dataHelper->getPageTitle();
        if ($pageTitle) {
            return $pageTitle;
        } else {
            return 'Event Calendar';
        }
    }
}
