<?php
namespace MageArray\Eventcalendar\Block\Event;

use MageArray\Eventcalendar\Helper\Data;
use MageArray\Eventcalendar\Model\CategoryFactory;
use MageArray\Eventcalendar\Model\EventFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Cms\Model\Template\FilterProvider;

/**
 * Class EventList
 * @package MageArray\Eventcalendar\Block\Event
 */
class EventList extends Template
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var Data
     */
    protected $_dataHelper;
    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;
    /**
     * @var EventFactory
     */
    protected $_eventFactory;
    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * EventList constructor.
     * @param Context $context
     * @param EventFactory $eventFactory
     * @param CategoryFactory $categoryFactory
     * @param DateTime $date
     * @param Data $dataHelper
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        EventFactory $eventFactory,
        CategoryFactory $categoryFactory,
        DateTime $date,
        Data $dataHelper,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_dataHelper = $dataHelper;
        $this->_eventFactory = $eventFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_filterProvider = $filterProvider;
        $this->date = $date;
    }

    /**
     * @return $this
     */
    public function getEvents()
    {
        $categoryId = $this->getRequest()->getParam('category');
        if (empty($categoryId)) {
            $collection = $this->_eventFactory->create()->getCollection()
                ->addFieldToFilter('status', 'enable');
        } else {
            $collection = $this->_eventFactory->create()->getCollection()
                ->addFieldToFilter('status', 'enable')
                ->addFieldToFilter('categories',
                    ['finset' => $categoryId]);
        }
        if (!$this->_dataHelper->getExpiredEventDisplay()) {
            $collection = $collection->addFieldToFilter('start_date_time',
                ['gteq' => date('Y-m-d H:i:s')]);
        }
        return $collection;
    }

    /**
     * @return mixed
     */
    public function getSelectCategory()
    {
        $categoryId = $this->getRequest()->getParam('category');
        return $categoryId;
    }

    /**
     * @param $description
     * @return string
     */
    public function getDescription($description)
    {
        $data = $this->_filterProvider
            ->getBlockFilter()->filter($description);
        $data = preg_replace('/\s+/', '', $data);
        return preg_replace("/'/", "\&#39;", $data);
    }

    /**
     * @return mixed
     */
    public function getPageUrl()
    {
        $pageUrl = $this->_dataHelper->getPageUrl();
        return $pageUrl;
    }

    /**
     * @param $id
     * @return string
     */
    public function getEventUrl($id)
    {
        return $this->getUrl($this->getPageUrl() . '/event/eventview',
            ['id' => $id]);
    }

    /**
     * @return string
     */
    public function getCurrentDate()
    {
        $date = $this->date->gmtDate('Y-m-d');
        return $date;
    }

    /**
     * @return $this
     */
    public function getCategories()
    {
        $categories = $this->_categoryFactory->create()->getCollection()
            ->addFieldToFilter('status', 'enable');
        return $categories;
    }

    /**
     * @return mixed
     */
    public function getEventView()
    {
        $mode = $this->getRequest()->getParam('mode');
        if (empty($mode)) {
            return $this->_dataHelper->getEventViewMode();
        } else {
            return $mode;
        }
    }

    /**
     * @return string
     */
    public function getPlaceHolderImage()
    {
        return $this->getViewFileUrl(
            'MageArray_Eventcalendar::image/event-placeholder.jpg'
        );
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $metaTitle = $this->_dataHelper->getMetaTitle();
        $metaKeywords = $this->_dataHelper->getMetaKeywords();
        $metaDescription = $this->_dataHelper->getMetaDescription();

        if ($metaTitle) {
            $this->pageConfig->getTitle()->set($metaTitle);
        } else {
            $this->pageConfig->getTitle()->set(__('Event Calendar'));
        }

        if ($metaKeywords) {
            $this->pageConfig->setKeywords($metaKeywords);
        } else {
            $this->pageConfig->setKeywords(__('Event Calendar'));
        }

        if ($metaDescription) {
            $this->pageConfig->setDescription($metaDescription);
        } else {
            $this->pageConfig->setDescription(__('Event Calendar'));
        }
        return parent::_prepareLayout();
    }
}