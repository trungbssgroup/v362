<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

/**
 * Class DateFormate
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer
 */
class DateFormate extends AbstractRenderer
{
    /**
     * DateFormate constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_authorization = $context->getAuthorization();
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $date = $this->_getValue($row);
        $dateTime = strtotime(
            $date
        ) ? $this->formatDate($date,
            \IntlDateFormatter::MEDIUM,
            true
        ) : __(
            'N/A'
        );
        return $dateTime;
    }
}
