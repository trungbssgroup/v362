<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event;

use MageArray\Eventcalendar\Model\EventFactory;
use MageArray\Eventcalendar\Model\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;

/**
 * Class GridExport
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event
 */
class GridExport extends Extended
{
    /**
     * @var EventFactory
     */
    protected $_eventFactory;
    /**
     * @var Status
     */
    protected $_status;

    /**
     * GridExport constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Status $status
     * @param EventFactory $eventFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Status $status,
        EventFactory $eventFactory,
        array $data = []
    ) {
        $this->_eventFactory = $eventFactory;
        $this->_status = $status;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('eventGrid');
        $this->setDefaultSort('event_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);

    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_eventFactory->create()->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'index' => 'title',
            ]
        );

        $this->addColumn(
            'start_date_time',
            [
                'header' => __('Start Date Time'),
                'index' => 'start_date_time',
                'type' => 'date',
                'renderer' => '\MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer\DateFormate',
                'width' => '150px',
            ]
        );

        $this->addColumn(
            'end_date_time',
            [
                'header' => __('End Date Time'),
                'index' => 'end_date_time',
                'type' => 'date',
                'renderer' =>
                    '\MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer\DateFormate',
                'width' => '150px',
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => $this->_status->getOptionArray(),
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'venue',
            [
                'header' => __('Venue'),
                'index' => 'venue',
            ]
        );

        $this->addColumn(
            'phone',
            [
                'header' => __('Phone'),
                'index' => 'phone',
            ]
        );

        $this->addColumn(
            'fax',
            [
                'header' => __('Fax'),
                'index' => 'fax',
            ]
        );

        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
            ]
        );

        $this->addColumn(
            'address',
            [
                'header' => __('Address'),
                'index' => 'address',
            ]
        );

        $this->addColumn(
            'url_key',
            [
                'header' => __('Url Key'),
                'index' => 'url_key',
            ]
        );

        $this->addColumn(
            'meta_title',
            [
                'header' => __('Meta Title'),
                'index' => 'meta_title',
            ]
        );

        $this->addColumn(
            'meta_keywords',
            [
                'header' => __('Meta Keywords'),
                'index' => 'meta_keywords',
            ]
        );

        $this->addColumn(
            'meta_description',
            [
                'header' => __('Meta Description'),
                'index' => 'meta_description',
            ]
        );

        $this->addExportType('*/*/exportCsv',
            __('CSV'));
        return parent::_prepareColumns();
    }
}

