<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event
 */
class Edit extends Container
{

    /**
     * Edit constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     *
     */
    protected function _construct()
    {

        $this->_objectId = 'event_id';
        $this->_blockGroup = 'MageArray_Eventcalendar';
        $this->_controller = 'adminhtml_event';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save Event'));
        $this->buttonList->update('delete', 'label', __('Delete Event'));
        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ],
                    ],
                ],
            ],
            10
        );
        if ($this->getRequest()->getParam('event_id')) {
            $duplicateUrl = $this->_urlBuilder->getUrl(
                'eventcalendar/event/duplicate',
                [
                    'event_id' => $this->getRequest()->getParam('event_id'),
                ]
            );
            $this->buttonList->add(
                'duplicate',
                [
                    'class' => 'save',
                    'label' => __('Duplicate'),
                    'onclick' => 'setLocation("' . $duplicateUrl . '")'
                ],
                12
            );
        }
    }

    /**
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            ['_current' => true, 'back' => 'edit', 'tab' => '{{tab_id}}']
        );
    }
}
