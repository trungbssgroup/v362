<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event;

use MageArray\Eventcalendar\Model\EventFactory;
use MageArray\Eventcalendar\Model\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;

/**
 * Class Grid
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event
 */
class Grid extends Extended
{

    /**
     * @var EventFactory
     */
    protected $_eventFactory;
    /**
     * @var Status
     */
    protected $_status;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Status $status
     * @param EventFactory $eventFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Status $status,
        EventFactory $eventFactory,
        array $data = []
    ) {
        $this->_eventFactory = $eventFactory;
        $this->_status = $status;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('eventGrid');
        $this->setDefaultSort('event_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_eventFactory->create()->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'event_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'event_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'width' => '10px',
            ]
        );
        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'index' => 'title',
            ]
        );
        $this->addColumn(
            'start_date_time',
            [
                'header' => __('Start Date Time'),
                'index' => 'start_date_time',
                'type' => 'date',
                'renderer' => '\MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer\DateFormate',
                'width' => '150px',
            ]
        );
        $this->addColumn(
            'end_date_time',
            [
                'header' => __('End Date Time'),
                'index' => 'end_date_time',
                'type' => 'date',
                'renderer' => '\MageArray\Eventcalendar\Block\Adminhtml\Event\Renderer\DateFormate',
                'width' => '150px',
            ]
        );
        $this->addColumn(
            'color',
            [
                'header' => __('Color'),
                'index' => 'color',
            ]
        );
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => $this->_status->getOptionArray(),
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'index' => 'status',
                'type' => 'action',
                'getter' => 'getId',
                'width' => '20px',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit',
                        ],
                        'field' => 'event_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false
            ]
        );
        $this->addExportType('*/*/exportCsv',
            __('CSV'));
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['event_id' => $row->getId()]);
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('event');
        $this->getMassactionBlock()->addItem('delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massDelete', ['' => '']),
            'confirm' => __('Are you sure?')
        ]);

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses,
            ['label' => 'Select Status', 'value' => '']);
        $this->getMassactionBlock()->addItem('status', [
            'label' => __('Change Status'),
            'url' => $this->getUrl('*/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
        ]);
        return $this;
    }
}
