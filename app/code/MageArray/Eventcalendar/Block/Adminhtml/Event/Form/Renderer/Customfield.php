<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Form\Renderer;

use Magento\Framework\Data\Form\Element\Image as ImageField;
use Magento\Framework\Data\Form\Element\Factory as ElementFactory;
use Magento\Framework\Data\Form\Element\CollectionFactory as ElementCollectionFactory;
use Magento\Framework\Escaper;
use MageArray\Eventcalendar\Helper\Data as EventImage;
use Magento\Framework\UrlInterface;

/**
 * Class Customfield
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Form\Renderer
 */
class Customfield extends ImageField
{

    /**
     * @var EventImage
     */
    protected $_imageModel;

    /**
     * Customfield constructor.
     * @param EventImage $imageModel
     * @param ElementFactory $factoryElement
     * @param ElementCollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        EventImage $imageModel,
        ElementFactory $factoryElement,
        ElementCollectionFactory $factoryCollection,
        Escaper $escaper,
        UrlInterface $urlBuilder,
        $data = []
    ) {
        $this->_imageModel = $imageModel;
        parent::__construct(
            $factoryElement,
            $factoryCollection,
            $escaper,
            $urlBuilder,
            $data
        );
    }

    /**
     * @return bool|string
     */
    protected function _getUrl()
    {
        $url = false;
        if ($this->getValue()) {
            $url = $this->_imageModel
                    ->getBaseUrlMedia() . 'event/' . $this->getValue();
        }
        return $url;
    }
}
