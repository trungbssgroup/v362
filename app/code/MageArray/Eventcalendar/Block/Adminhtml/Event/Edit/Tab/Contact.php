<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;

/**
 * Class Contact
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab
 */
class Contact extends Generic
{
    /**
     * Contact constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('event_post');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->
        addFieldset('base_fieldset', ['legend' => __('Contact Information')]);
        if ($model->getId()) {
            $fieldset->addField('event_id', 'hidden', ['name' => 'event_id']);
        }

        $fieldset->addField(
            'venue',
            'text',
            [
                'label' => __('Venue'),
                'title' => __('Venue'),
                'required' => false,
                'name' => 'venue',
            ]
        );

        $fieldset->addField(
            'organizer_name',
            'text',
            [
                'label' => __('Organizer Name'),
                'title' => __('Organizer Name'),
                'required' => false,
                'name' => 'organizer_name',
            ]
        );

        $fieldset->addField(
            'phone',
            'text',
            [
                'label' => __('Phone No'),
                'title' => __('Phone No'),
                'required' => false,
                'name' => 'phone',
            ]
        );

        $fieldset->addField(
            'fax',
            'text',
            [
                'label' => __('Fax'),
                'title' => __('Fax'),
                'required' => false,
                'name' => 'fax',
            ]
        );

        $fieldset->addField(
            'email',
            'text',
            [
                'label' => __('Email'),
                'title' => __('Email'),
                'required' => false,
                'name' => 'email',
            ]
        );

        $fieldset->addField(
            'address',
            'textarea',
            [
                'label' => __('Address'),
                'title' => __('Address'),
                'required' => false,
                'name' => 'address',
            ]
        );
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
