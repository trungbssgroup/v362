<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab;

use MageArray\Eventcalendar\Model\Categories\Categories;
use MageArray\Eventcalendar\Model\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Class Visibility
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab
 */
class Visibility extends Generic
{
    /**
     * @var Status
     */
    protected $_status;
    /**
     * @var Categories
     */
    protected $_categories;
    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * Visiblity constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Store $systemStore
     * @param Status $status
     * @param Categories $categories
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        Status $status,
        Categories $categories,
        array $data = []
    ) {
        $this->_categories = $categories;
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('event_post');
        $isElementDisabled = false;
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form
            ->addFieldset('base_fieldset', ['legend' => __('Visibility')]);

        if ($model->getId()) {
            $fieldset
                ->addField('event_id', 'hidden', ['name' => 'event_id']);
        }

        $fieldset->addField(
            'categories',
            'multiselect',
            [
                'name' => 'categories[]',
                'label' => __('Categories'),
                'title' => __('Categories'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'values' => $this->_categories->toOptionArray()
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'required' => true,
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'options' => $this->_status->getOptionArray(),
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                'store_id',
                'multiselect',
                [
                    'name' => 'stores[]',
                    'label' => __('Store View'),
                    'title' => __('Store View'),
                    'required' => true,
                    'values' => $this->_systemStore->
                    getStoreValuesForForm(false, true)
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                [
                    'name' => 'stores[]',
                    'value' => $this->_storeManager->getStore(true)->getId()
                ]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
