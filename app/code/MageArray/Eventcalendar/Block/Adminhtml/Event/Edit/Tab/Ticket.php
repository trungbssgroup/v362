<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;

/**
 * Class Ticket
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab
 */
class Ticket extends Generic
{
    /**
     * Ticket constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('event_post');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset('base_fieldset',
            ['legend' => __('Ticket Information')]);
        if ($model->getId()) {
            $fieldset->addField('event_id', 'hidden', ['name' => 'event_id']);
        }

        $fieldset->addField(
            'ticket_product_sku',
            'text',
            [
                'label' => __('Ticket Product SKU'),
                'title' => __('Ticket Product SKU'),
                'required' => false,
                'name' => 'ticket_product_sku',
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
