<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Cms\Model\Wysiwyg\Config;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;

/**
 * Class General
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab
 */
class General extends Generic
{
    /**
     * @var Config
     */
    protected $_wysiwygConfig;

    /**
     * General constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('event_post');
        $isElementDisabled = false;
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form
            ->addFieldset('base_fieldset',
                ['legend' => __('General Information')]
            );
        if ($model->getId()) {
            $fieldset->addField('event_id', 'hidden', ['name' => 'event_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'name' => 'title',
            ]
        );

        $wysiwygConfig = $this->_wysiwygConfig
            ->getConfig(['tab_id' => $this->getTabId()]);

        $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => true,
                'style' => 'height:10em;',
                'disabled' => $isElementDisabled,
                'config' => $wysiwygConfig
            ]
        );
        $fieldset->addType(
            'image',
            '\MageArray\Eventcalendar\Block\Adminhtml\Event\Form\Renderer\Customfield'
        );

        $fieldset->addField(
            'event_image',
            'image',
            [
                'name' => 'event_image',
                'required' => true,
                'label' => __('Event Image'),
                'title' => __('Event Image'),
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );

        $timeFormat = $dateFormat = $this->_localeDate->getTimeFormat();

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::SHORT
        );

        $fieldset->addField(
            'start_date_time',
            'date',
            [
                'name' => 'start_date_time',
                'label' => __('Start Date Time'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'required' => true,
                'disabled' => $isElementDisabled,
                'time' => true,
                'class' => 'validate-date validate-date-range date-range-custom_theme-from'
            ]
        );

        $fieldset->addField(
            'end_date_time',
            'date',
            [
                'name' => 'end_date_time',
                'label' => __('End Date Time'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'required' => true,
                'disabled' => $isElementDisabled,
                'time' => true,
                'class' => 'validate-date validate-date-range date-range-custom_theme-from'
            ]
        );

        $fieldset->addField(
            'color',
            'text',
            [
                'label' => __('Color'),
                'title' => __('Color'),
                'class' => 'jscolor {hash:true,refine:false}',
                'required' => true,
                'name' => 'color',
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
