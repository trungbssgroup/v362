<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit;

/**
 * Class Tabs
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit
 */

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('event_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Event Information'));
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'main_section',
            [
                'label' => __('General Information'),
                'title' => __('General Information'),
                'content' => $this->getLayout()
                    ->createBlock('MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab\General')->toHtml(),
                'active' => true
            ]
        );

        $this->addTab(
            'main_section2',
            [
                'label' => __('Contact Information'),
                'title' => __('Contact Information'),
                'content' => $this->getLayout()
                    ->createBlock('MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab\Contact')
                    ->toHtml(),
                'active' => false
            ]
        );

        $this->addTab(
            'main_section3',
            [
                'label' => __('Visibility'),
                'title' => __('Visibility'),
                'content' => $this->getLayout()
                    ->createBlock('MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab\Visibility')
                    ->toHtml(),
                'active' => false
            ]
        );

        $this->addTab(
            'main_section4',
            [
                'label' => __('Search Engine Optimization'),
                'title' => __('Search Engine Optimization'),
                'content' => $this->getLayout()
                    ->createBlock('MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab\Seo')
                    ->toHtml(),
                'active' => false
            ]
        );

        $this->addTab(
            'main_section5',
            [
                'label' => __('Ticket Information'),
                'title' => __('Ticket Information'),
                'content' => $this->getLayout()
                    ->createBlock(
                        'MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab\Ticket'
                    )
                    ->toHtml(),
                'active' => false
            ]
        );

        $this->addTab(
            'related_events_section',
            [
                'label' => __('Related Events'),
                'url' => $this->getUrl('eventcalendar/event/relatedEvents',
                    ['_current' => true]),
                'class' => 'ajax',
            ]
        );
    }
}
