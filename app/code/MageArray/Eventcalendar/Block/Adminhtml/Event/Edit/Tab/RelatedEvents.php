<?php
namespace MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Helper\Data;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Registry;

/**
 * Class RelatedEvents
 * @package MageArray\Eventcalendar\Block\Adminhtml\Event\Edit\Tab
 */
class RelatedEvents extends Extended implements TabInterface
{

    /**
     * @var Registry|null
     */
    protected $_coreRegistry = null;
    /**
     * @var Status
     */
    protected $_status;

    /**
     * RelatedEvents constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Status $status
     * @param Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Status $status,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->_status = $status;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('related_event_section');
        $this->setDefaultSort('event_id');
        $this->setUseAjax(true);
        if ($this->getEvent() && $this->getEvent()->getId()) {
            $this->setDefaultFilter(['in_events' => 1]);
        }
        if ($this->isReadonly()) {
            $this->setFilterVisibility(false);
        }
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->_coreRegistry->registry('related_event_model');
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_events') {
            $eventIds = $this->_getSelectedEvents();
            if (empty($eventIds)) {
                $eventIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()
                    ->addFieldToFilter('event_id', ['in' => $eventIds]);
            } else {
                if ($eventIds) {
                    $this->getCollection()
                        ->addFieldToFilter('event_id', ['nin' => $eventIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }


    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->getEvent()->getCollection();

        $collection->addFieldToFilter(
            'event_id',
            ['neq' => $this->getEvent()->getId()]
        );

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return bool
     */
    public function isReadonly()
    {
        return false;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        if (!$this->isReadonly()) {
            $this->addColumn(
                'in_events',
                [
                    'type' => 'checkbox',
                    'name' => 'in_events',
                    'values' => $this->_getSelectedEvents(),
                    'align' => 'center',
                    'index' => 'event_id',
                    'header_css_class' => 'col-select',
                    'column_css_class' => 'col-select'
                ]
            );
        }

        $this->addColumn(
            'event_id_grid',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'event_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'title_grid',
            [
                'header' => __('Title'),
                'index' => 'title',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );
        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'name' => 'position',
                'type' => 'number',
                'validate_class' => 'validate-number',
                'index' => 'position',
                'editable' => true,
                'edit_only' => false,
                'sortable' => false,
                'filter' => false,
                'header_css_class' => 'col-position',
                'column_css_class' => 'col-position'
            ]
        );
        return parent::_prepareColumns();
    }

    /**
     * @return mixed|string
     */
    public function getGridUrl()
    {
        return $this->getData(
            'grid_url'
        ) ? $this->getData(
            'grid_url'
        ) : $this->getUrl(
            'eventcalendar/event/relatedEventsGrid',
            ['_current' => true]
        );
    }

    /**
     * @return array
     */
    protected function _getSelectedEvents()
    {
        $events = $this->getEventsRelated();
        if (!is_array($events)) {
            $events = array_keys($this->getSelectedEvents());
        }
        return $events;
    }

    /**
     * @return array
     */
    public function getSelectedEvents()
    {
        $events = [];
        foreach ($this->_coreRegistry
                     ->registry('related_event_model')
                     ->getRelatedEvents() as $event) {
            $events[$event->getId()] = ['position' => $event->getPosition()];
        }
        return $events;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Related Events');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Related Events');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
