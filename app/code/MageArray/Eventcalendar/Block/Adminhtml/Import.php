<?php
namespace MageArray\Eventcalendar\Block\Adminhtml;
use Magento\Backend\Block\Template;

/**
 * Class Import
 * @package MageArray\Eventcalendar\Block\Adminhtml
 */
class Import extends Template
{
    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->getViewFileUrl('MageArray_Eventcalendar::Events.csv');
    }
}
