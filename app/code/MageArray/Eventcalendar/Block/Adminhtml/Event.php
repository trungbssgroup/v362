<?php
namespace MageArray\Eventcalendar\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Class Event
 * @package MageArray\Eventcalendar\Block\Adminhtml
 */
class Event extends Container
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_event';
        $this->_blockGroup = 'MageArray_Eventcalendar';
        $this->_headerText = __('Manage Events');
        $this->_addButtonLabel = __('Add New Event');
        parent::_construct();
    }
}
