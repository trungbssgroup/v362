<?php
namespace MageArray\Eventcalendar\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Category
 * @package MageArray\Eventcalendar\Model
 */
class Category extends AbstractModel
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('MageArray\Eventcalendar\Model\ResourceModel\Category');
    }
}
