<?php
namespace MageArray\Eventcalendar\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Event
 * @package MageArray\Eventcalendar\Model\ResourceModel
 */
class Event extends AbstractDb
{
    /**
     * @var null
     */
    protected $connection = null;

    /**
     * Event constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('magearray_eventcalendar_event', 'event_id');
    }

    /**
     * @param $urlKey
     * @return string
     */
    public function checkUrlKey($urlKey)
    {
        $select = $this->getLoadByUrlKeySelect($urlKey);

        $select->reset(\Zend_Db_Select::COLUMNS)
            ->columns('magearray_eventcalendar_event.event_id')
            ->limit(1);
        return $this->getConnection()->fetchOne($select);
    }

    /**
     * @param $urlKey
     * @return \Magento\Framework\DB\Select
     */
    protected function getLoadByUrlKeySelect($urlKey)
    {
        $select = $this->getConnection()
            ->select()
            ->from('magearray_eventcalendar_event')
            ->where('magearray_eventcalendar_event.url_key = ?',
                $urlKey
            );
        return $select;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface|null
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resources->getConnection('core_write');
        }
        return $this->connection;
    }

    /**
     * @param AbstractModel $object
     * @param mixed $value
     * @param null $field
     * @return $this
     */
    public function load(AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && empty($field)) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * @param AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(AbstractModel $object)
    {
        if ($object->getId()) {
            $relatedEvents = $this->getRelatedEventIds($object->getId());
            $object->setRelatedEventIds($relatedEvents);
        }

        return parent::_afterLoad($object);
    }

    /**
     * @param $eventId
     * @return array
     */
    public function getRelatedEventIds($eventId)
    {
        $adapter = $this->getConnection();

        $select = $adapter->select()->from(
            $this->getTable('magearray_eventcalendar_event_relatedevent'),
            'related_id'
        )->where(
            'event_id = ?',
            (int)$eventId
        );

        return $adapter->fetchCol($select);
    }
}