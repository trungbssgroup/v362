<?php
namespace MageArray\Eventcalendar\Model\ResourceModel\Category;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\Eventcalendar\Model\ResourceModel\Category
 */
class Collection extends AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\Eventcalendar\Model\Category',
            'MageArray\Eventcalendar\Model\ResourceModel\Category'
        );
    }
}