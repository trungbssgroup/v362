<?php
namespace MageArray\Eventcalendar\Model\ResourceModel\Event;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package MageArray\Eventcalendar\Model\ResourceModel\Event
 */
class Collection extends AbstractCollection
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init(
            'MageArray\Eventcalendar\Model\Event',
            'MageArray\Eventcalendar\Model\ResourceModel\Event'
        );
    }
}