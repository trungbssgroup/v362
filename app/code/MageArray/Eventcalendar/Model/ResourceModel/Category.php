<?php
namespace MageArray\Eventcalendar\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Category
 * @package MageArray\Eventcalendar\Model\ResourceModel
 */
class Category extends AbstractDb
{
    /**
     * @var null
     */
    protected $connection = null;

    /**
     * Category constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('magearray_eventcalendar_category', 'category_id');
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface|null
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resources->getConnection('core_write');
        }
        return $this->connection;
    }
}