<?php
namespace MageArray\Eventcalendar\Model\Categories;

use MageArray\Eventcalendar\Model\CategoryFactory;
use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Categories
 * @package MageArray\Eventcalendar\Model\Categories
 */
class Categories extends Template
    implements ArrayInterface
{
    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * Categories constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory
    ) {
        parent::__construct($context);
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        $categoryData = [];
        $categoryModel = $this->_categoryFactory->create();
        $categoryDetail = $categoryModel->getCollection();
        foreach ($categoryDetail as $detail) {
            $categoryData[$detail['category_id']] = $detail['title'];
        }
        return $categoryData;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }
}
