<?php
namespace MageArray\Eventcalendar\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Event
 * @package MageArray\Eventcalendar\Model
 */
class Event extends AbstractModel
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('MageArray\Eventcalendar\Model\ResourceModel\Event');
    }

    /**
     * @param $urlKey
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkUrlKey($urlKey)
    {
        return $this->_getResource()->checkUrlKey($urlKey);
    }

    /**
     * @return $this
     */
    public function getRelatedEvents()
    {
        $collection = $this->getCollection()
            ->addFieldToFilter(
                'main_table.event_id',
                ['neq' => $this->getId()]
            );
        $collection->getSelect()->joinLeft(
            [
                're' => $this->getResource()
                    ->getTable('magearray_eventcalendar_event_relatedevent')
            ],
            'main_table.event_id = re.related_id',
            ['position']
        )->where(
            're.event_id = ?',
            $this->getId()
        );
        return $collection;
    }

    /**
     * @return $this
     */
    public function beforeSave()
    {
        $this->_cacheManager->clean();
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlKey()
    {

        if ($this->getData('url_key')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $pageUrl = $objectManager->create('MageArray\Eventcalendar\Helper\Data')->getPageUrl();
            $urlInterface = $objectManager->get('Magento\Framework\UrlInterface');
            return $urlInterface->getUrl($pageUrl . '/' . $this->getData('url_key'));
        } else {
            return '';
        }
    }
}
