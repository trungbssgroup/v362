<?php
namespace MageArray\Eventcalendar\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Visible
 * @package MageArray\Eventcalendar\Model\Config\Source
 */
class Visible implements ArrayInterface
{
    /**
     *
     */
    const CALENDARVIEW = 'calendar';
    /**
     *
     */
    const LISTVIEW = 'list';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::CALENDARVIEW => __('Calendar'),
            self::LISTVIEW => __('List')
        ];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {

        $options = self::getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
