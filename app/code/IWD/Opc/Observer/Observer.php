<?php

namespace IWD\Opc\Observer;


use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote\Address;

use \Magento\Checkout\Model\Session as CheckoutSession;


class Observer implements ObserverInterface
{

    protected $checkoutSession;


  public function __construct(CheckoutSession $checkoutSession)
  {
  	    $this->checkoutSession = $checkoutSession;
  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
       
       $quote = $this->checkoutSession->getQuote();

        $shippingAddress = $quote->getShippingAddress();

        if(!$shippingAddress->getCountryId()){
            $shippingAddress->setCountryId('FR'); // Default country for calculate shipping rates if no one given
        }

        

        if(!$shippingAddress->getShippingMethod()){
            $shippingAddress->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod('freeshipping_freeshipping'); // Default Shipping method
        }

       

  }
}