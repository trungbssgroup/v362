# Module UB Data Migration Professional
>This is a Magento 2 module, allows migrate some important data from Magento 1.x to Magento 2.x

### Author: [UberTheme](http://www.ubertheme.com)

### Allow Features:
+ [x] Migrate Web sites, Stores, Store views
+ [x] Migrate Product Attribute Sets, Product Attribute Groups, Product Attributes
+ [x] Migrate Product Categories
+ [x] Migrate Products
+ [x] Migrate Customer Groups and Customers
+ [x] Migrate Sales Data: 
    + Sales Orders
    + Sales Quote
    + Sales Payments
    + Sales Invoices
    + Sales Shipments 
    + Sales Credit Memo
    + Sales Rules & Coupons 
+ [x] Migrate Product Reviews, Ratings data
+ [x] Migrate Tax Rules, Tax Zones and Tax Rates
+ [x] Catalog Rules 
+ [x] Email Templates and Newsletter Data
    + Email Templates
    + Newsletter Templates
    + Newsletter Queue
    + Newsletter Subscribers
    + Newsletter Problem Reports
+ [x] Migrate System Increment IDs

### Compatible:
    + Magento CE 1.x: 1.6.x, 1.7.x, 1.8.x, 1.9.x
    + Magento 2 CE 2.0.0 and later

### Prepare (precondition):
- Install a Magento 2 fresh website (without data sample)
    + Download Magento Community Edition 2.0.x from below link: https://www.magentocommerce.com/download
    + Follow [Installation guide](http://devdocs.magento.com/guides/v2.0/install-gde/install-quick-ref.html) to Install a Magento 2 fresh website
- Enable the **[php sqlite](http://php.net/manual/en/sqlite.installation.php)** in your server (_required_)
- Make write permission for `pub` folder at path `PATH_YOUR_MAGENTO_2/pub`

### How To Install:

#### Option 1: Install from zip file downloaded (manual install) 
- Extract the zip file you have downloaded. Copy the `app` folder and paste to root folder of your Magento 2 fresh (_you have to select merge folders/files option_).
- Open your Terminal window, go to your Magento 2 folder and run below commands to install this module:
    + `php -f bin/magento module:enable --clear-static-content Ubertheme_Ubdatamigration`
    + `php -f bin/magento setup:upgrade`
    + `php -f bin/magento cache:clean` (_optional_)

#### Option 2: Install via Composer:
- Open your terminal window, go to your Magento 2 folder and run below command:
    + `composer config repositories.ubdatamigration-pro vcs git@bitbucket.org:ubertheme/module-ubdatamigration-pro.git`
    + `composer require ubertheme/module-ubdatamigration-pro`
    + `php -f bin/magento module:enable --clear-static-content Ubertheme_Ubdatamigration`
    + `php -f bin/magento setup:upgrade`
    + `php -f bin/magento cache:clean` (_optional_)
- Successfully screenshots:
> Coming soon...

#### For Nginx Server only:
 + After install this tool. Add more bellow config to your nginx.conf:
 `include path_to_your_magento2/pub/ub-tool/nginx.conf;`
 
 + Example content in your nginx.conf:
    ```
     server {
        listen 80;
        #...other configs
      
        #include config file for ub-tool
        include path_to_your_magento2/pub/ub-tool/nginx.conf;
     }
     ```
 + Restart your Nginx server.
 
### How To Use:

#### 1 - Prepare to migrate data:
- Set maintenance mode for your Magento 1
- Off all cron jobs related your Magento 1

#### 2 - Login to back-end of your Magento 2, follow step by step with UI of this module as below screenshots:
- Step 1: Database settings
![step 1](http://i.imgur.com/Q90IxXi.png)

- Step 2: Migrate Websites, Stores
![step 2](http://i.imgur.com/Ht0FnPp.png)

- Step 3: Migrate Attributes 
![step 3](http://i.imgur.com/UtNvIkN.png)

- Step 4: Migrate Categories
![step 4](http://i.imgur.com/z4Vjrpk.png)

- Step 5: Migrate Products
![step 5](http://i.imgur.com/sODJ56R.png)

- Step 6: Migrate Customers
![step 6](http://i.imgur.com/zv0zrZk.png)

- Step 7: Migrate Sales Data
![step 7](http://i.imgur.com/clj8lPa.png)

- Step 8: Migrate Reviews & Ratings Data
![step 8](http://i.imgur.com/hRD5OWZ.png)

- Step 9: Migrate Other Data
![step 9](http://i.imgur.com/93J2HZR.png)

#### 3 - Finish (_required_)
**To finish the data migration from Magento 1.x to Magento 2.x, you have to do some tasks below:**

- Copy media files
    + Copy the folder at PATH_YOUR_MAGENTO_1\media\catalog and paste replace to PATH_YOUR_MAGENTO_2\pub\media\
    + Copy the folder at PATH_YOUR_MAGENTO_1\media\downloadable and paste replace to PATH_YOUR_MAGENTO_2\pub\media\
    + Make recursive write permission to "catalog" and "downloadable" folders which you have just copied.

- Re-save all the Attribute Sets migrated:
    In back-end of your Magento 2 go to Stores/Attributes/Attribute menu navigation. 
    Open the Attribute Set, edit information of it if needed and click the save button

- Re-Index the data: In your terminal window, go to your Magento 2 folder and run below command:
    `php -f bin/magento indexer:reindex`

- Clean Magento 2 cache: In your terminal window, go to your Magento 2 folder and run below command:
    `php -f bin/magento cache:clean`

- Upgrade Password Hash (__This is optional task for more security__): In your terminal window, go to your Magento 2 folder and run below command:
    `php -f bin/magento customer:hash:upgrade`

### 4 - Security Note:
- If you install this tool in your production site, please have this module disabled after you finish the data migration following steps below:
    + Remove the folder: `pub/ub-tool` (_or you can move this folder to another folder: var/_)
    + Disable this module by running the command: `php -f bin/magento module:disable --clear-static-content Ubertheme_Ubdatamigration`

### 3 - Let’s discover Magento 2 with your data migrated by URL:
    http://your_magento2_url/