<?php
namespace Bss\CustomField\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
	/**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    private $attributeSetFactory;

    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
    	\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
    	\Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
    	)
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }

	/**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2') < 0) {
 
	    	$customerEntity = $customerSetup->getEavConfig()->getEntityType('customer_address');
	        $attributeSetId = $customerEntity->getDefaultAttributeSetId();

	        $attributeSet = $this->attributeSetFactory->create();
	        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

	        $customerSetup->addAttribute('customer_address', 'suite', [
	            'type' => 'varchar',
	            'label' => 'Suite/Appt',
	            'input' => 'text',
	            'required' => false,
	            'visible' => true,
	            'visible_on_front' => true,
	            'user_defined' => false,
	            'sort_order' => 43,
	            'position' => 43,
	            'system' => 0,
	        ]);

	        $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'suite')
	            ->addData([
	                'attribute_set_id' => $attributeSetId,
	                'attribute_group_id' => $attributeGroupId,
	                'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address','customer_address'],
	            ]);
	        $attribute->save();
	    }

        $setup->endSetup();
    }
}	